#include<cstring>
#include<vector>

#include <LeMonADE/core/Ingredients.h>
#include <LeMonADE/core/ConfigureSystem.h>
#include <LeMonADE/feature/FeatureMoleculesIO.h>
#include <LeMonADE/utility/DepthIteratorPredicates.h>
#include <LeMonADE/utility/TaskManager.h>
#include <LeMonADE/utility/MonomerGroup.h>
#include <LeMonADE/utility/RandomNumberGenerators.h>
#include <LeMonADE/analyzer/AnalyzerWriteBfmFile.h>
#include <LeMonADE/analyzer/AnalyzerWriteBfmFileSubGroup.h>
#include <LeMonADE/analyzer/AnalyzerRadiusOfGyration.h>
#include <LeMonADE/updater/UpdaterSimpleSimulator.h>
#include <LeMonADE/updater/UpdaterReadBfmFile.h>

#include <LeMonADE-Extensions/features/FeatureExternalPotential.h>
#include <LeMonADE-Extensions/utilities/CommandlineParser.h>
#include <LeMonADE-Extensions/updaters/UpdaterLinearChainCreator.h>
#include <LeMonADE-Extensions/analyzers/UnmovedMonomersAnalyzer.h>
#include <LeMonADE-Extensions/analyzers/LatticeOccupationAnalyzer.h>

#include <LeMonADE-ImplicitMembrane/FeatureUmbrellaPotential.h>
#include <LeMonADE-ImplicitMembrane/UpdaterAdaptiveUmbrellaSampling.h>
#include <LeMonADE-ImplicitMembrane/Potentials.h>
#include <LeMonADE-ImplicitMembrane/UpdaterCreateABCP.h>

int main(int argc, char* argv[])
{

	////////////////////default values for command line parameters /////////////
	
	uint64_t upper_simulation_limit=50000000; //5e7mcs
	uint64_t save_interval=10000;
	uint32_t histogram_update_interval=100;
	uint32_t bias_update_interval=100000;
	uint32_t equilibration_steps=1000000; //1e6mcs
	
	int32_t box_x=64;
	int32_t box_y=64;
	int32_t box_z=64;
	
	double convergence_flatness=0.1;
	double convergence_histogram=0.02;
	size_t nBinsHistogram=box_z;
	double binCountThreshold=10000.0;
	
	
	//external potential settings
	double epsilonH=0.0; //magnitude of hydrophobic potential in kT
	double epsilonD=0.0; //magnitude of density potential in kT
	double epsilonQ=0.0;
	int32_t hydrophobicWidth=12; //width of hydrophobic potential
	int32_t densityWidth=10; //width of hydrophobic potential
	int32_t chargeCenter=4;
	int32_t chargeWidth=4; //width of charge potential around centerQ
	
	
	std::vector<double> hydrophobicity(4,0.0); //hydrophobicities of monomer types
	
	//input/output settings
	std::string outputFilename;
	std::string inputFilename;
	
	
	try{	
		//seed global random number generators
		RandomNumberGenerators randomNumbers;
		randomNumbers.seedAll();
		
		//////////////add command line arguments /////////////////////////////////
		CommandLineParser cmd;
		cmd.addOption("--max_mcs",1,"simulation runs to this mcs (default 1e8)");
		cmd.addOption("--save_interval",1,"mcs interval for saving configurations (default 50000)");
		cmd.addOption("--equilibration",1,"mcs for equilibration (default 1e7)");
		cmd.addOption("--box",3,"size of simulation box. --box x y z. default 64 64 64");
		cmd.addOption("--hydrophobic_pot",2,"set width and depth of hydrophob. pot.,default 12, 0.0");
		cmd.addOption("--density_pot",2,"set width and depth of density pot.,default 10, 0.0");
		cmd.addOption("--charge_pot",3,"set center,width and depth of charge pot.,default 4, 4, 0.0");
		cmd.addOption("--abcp",6,"add block copolyer to system. syntax: nPolymers type1 n1(block) type2 n2(block) nblocks, default 0 polymers");		
		cmd.addOption("--hydrophobicity",4,"adjust relative hydrophobicity of types 1-4. Default 0.0");
		cmd.addOption("--startconfig",1,"use file as start configuration. other conflicting options (e.g. box) are ignored");
		cmd.addOption("--umbrella_intervals",2,"histogram and bias update intervals, default 100,100000");
		cmd.addOption("--umbrella_convergence",3,"variance and flattness convergence values and binCountThreshold, default 0.02,0.1,10000.0");
		cmd.addOption("--umbrella_bins",1,"number of bins for umbrella potential and histogram, default boxZ");
		cmd.addOption("--help",0,"display help message");
		
		if ( argc < 2 || std::string(argv[1])=="--help" )
		  {std::cout<<"usage: program outputFilename [options]\n";cmd.displayHelp();exit(0);}
		
		//get output filename as first argument
		outputFilename=std::string(argv[1]);
		//////////////parse command line arguments /////////////////////////////////
		cmd.parse(argv+2,argc-2);
		cmd.getOption("--max_mcs",upper_simulation_limit);
		cmd.getOption("--save_interval",save_interval);
		cmd.getOption("--equilibration",equilibration_steps);
		cmd.getOption("--box",box_x,0);
		cmd.getOption("--box",box_y,1);
		cmd.getOption("--box",box_z,2);
		cmd.getOption("--hydrophobic_pot",hydrophobicWidth,0);
		cmd.getOption("--hydrophobic_pot",epsilonH,1);
		cmd.getOption("--density_pot",densityWidth,0);
		cmd.getOption("--density_pot",epsilonD,1);
		
		cmd.getOption("--charge_pot",chargeCenter,0);
		cmd.getOption("--charge_pot",chargeWidth,1);
		cmd.getOption("--charge_pot",epsilonQ,2);
		
		cmd.getOption("--umbrella_intervals",histogram_update_interval,0);
		cmd.getOption("--umbrella_intervals",bias_update_interval,1);
		cmd.getOption("--umbrella_convergence",convergence_histogram,0);
		cmd.getOption("--umbrella_convergence",convergence_flatness,1);
		cmd.getOption("--umbrella_convergence",binCountThreshold,2);
		cmd.getOption("--umbrella_bins",nBinsHistogram);
		
		for(size_t n=0;n<4;n++)
			cmd.getOption("--hydrophobicity",hydrophobicity[n],n);
		if(cmd.getOption("--help"))
		  {std::cerr<<"usage: program filename [options]\n";cmd.displayHelp();exit(0);}
		
		
		
		
		
		
		//////////// done getting command line parameters....start setting up the system ///
		///////////////////////////////////////////////////////////////////////
		
		//define ingredients
		typedef LOKI_TYPELIST_3(FeatureMoleculesIO,
					FeatureExternalPotential<ChargedImplicitMembranePotential>,
					FeatureUmbrellaPotential
       				) Features;

		typedef ConfigureSystem<VectorInt3,Features> Config;
		typedef Ingredients<Config> Ing;
		Ing myIngredients;
		
		
		TaskManager taskManager;
		
		//////// check validity of lattice occupation during setup and simulation ///
		//LatticeOccupationAnalyzer<Ing>* latticeCheck=new LatticeOccupationAnalyzer<Ing>(myIngredients);
		
		//this is used later when creating polymers and analyzing them
		std::vector<MonomerGroup<typename Ing::molecules_type> > polymersGroupVector;
		
		myIngredients.modifyPotential().setWidthH(hydrophobicWidth);
		myIngredients.modifyPotential().setWidthD(densityWidth);
		myIngredients.modifyPotential().setDepthH(epsilonH);
		myIngredients.modifyPotential().setDepthD(epsilonD);
		myIngredients.modifyPotential().setHydrophobicities(hydrophobicity);
		
		myIngredients.modifyPotential().setDepthQ(epsilonQ);
		myIngredients.modifyPotential().setWidthQ(chargeWidth);
		myIngredients.modifyPotential().setCenterQ(chargeCenter);
		
		myIngredients.modifyUmbrellaPotential().reset(0.0,box_z,nBinsHistogram);
		
		if(cmd.getOption("--startconfig"))
		{
		  std::string inputFile;
		  cmd.getOption("--startconfig",inputFile);
		  std::cout<<"using file "<<inputFilename<<" as start configuration.\n";

		  UpdaterReadBfmFile<Ing>* fileReader=
		    new UpdaterReadBfmFile<Ing>(inputFilename,
						myIngredients,
						UpdaterReadBfmFile<Ing>::READ_LAST_CONFIG_FAST);

		  fileReader->initialize();
			
		  /*check if upper simulation limit is lower than the max mcs in the file*/
		  if(fileReader->getMaxAge()>=upper_simulation_limit)
		    std::cerr<<"requested upper mcs limit for simulation is lower than "
			     <<"the highest mcs in the file.\n";
			
		  taskManager.addUpdater(fileReader,0);
		  
		  
		}
		//if constructing new system
		else
		{
			/////////// set system parameters //////////////////////////////////////
			/*set up the box*/
			myIngredients.setBoxX(box_x);
			myIngredients.setBoxY(box_y);
			myIngredients.setBoxZ(box_z);
			myIngredients.setPeriodicX(1);
			myIngredients.setPeriodicY(1);
			myIngredients.setPeriodicZ(1);
			/*add the bondset*/
			myIngredients.modifyBondset().addBFMclassicBondset();
			
			/*synchronize to create lattice*/
			myIngredients.synchronize(myIngredients);
			//latticeCheck->initialize();
			
			
			if(cmd.getOption("--abcp"))
			{
				uint32_t n_polymers;
				cmd.getOption("--abcp",n_polymers,0);
				
				uint32_t lenA,lenB,nblocks;
				int32_t typeA,typeB;
				
				cmd.getOption("--abcp",typeA,1);
				cmd.getOption("--abcp",lenA,2);
				cmd.getOption("--abcp",typeB,3);
				cmd.getOption("--abcp",lenB,4);
				cmd.getOption("--abcp",nblocks,5);
				/*now put the polymer in the system */
				UpdaterCreateABCP<Ing> polymerCreator(myIngredients,
								      typeA,
					  typeB,
					  lenA,
					  lenB,
					  nblocks,
					  n_polymers
				);
				
				polymerCreator.initialize();
				polymerCreator.execute();
			}
			
			//latticeCheck->execute();
			
			myIngredients.synchronize(myIngredients);
			
		}

		
		fill_connected_groups(myIngredients.getMolecules(),
				      polymersGroupVector,
				      MonomerGroup<typename Ing::molecules_type>(myIngredients.getMolecules()),
				      alwaysTrue());
			
		
		
		//this is run only once at the beginning for equilibration.
		taskManager.addUpdater(new UpdaterSimpleSimulator<Ing,MoveLocalSc>(myIngredients,
										   equilibration_steps),
				       0);

		taskManager.addUpdater(new UpdaterAdaptiveUmbrellaSampling<Ing,MoveLocalSc>(myIngredients,
											    save_interval,
									      bias_update_interval,
									      histogram_update_interval,
									      convergence_histogram,
									      convergence_flatness,
									      binCountThreshold,
									      upper_simulation_limit),
				       1);
		
		/*add system consistency checker*/
		//taskManager.addAnalyzer(latticeCheck,100);
		taskManager.addAnalyzer(new UnmovedMonomersAnalyzer<Ing>(myIngredients),100);
		taskManager.addAnalyzer(new AnalyzerRadiusOfGyration<Ing>(myIngredients,
									  "polymer"),
					1);
		
		
		taskManager.addAnalyzer(new AnalyzerWriteBfmFile<Ing>(outputFilename,
								      myIngredients,
								      AnalyzerWriteBfmFile<Ing>::NEWFILE),
					1);
		
		/*init and run the simulation*/
		taskManager.initialize();
		taskManager.run();
		taskManager.cleanup();
	}
	catch(std::runtime_error& e){std::cerr<<e.what()<<std::endl;}
	catch(std::exception& e){std::cerr<<e.what()<<std::endl;}
	
	return 0;
}



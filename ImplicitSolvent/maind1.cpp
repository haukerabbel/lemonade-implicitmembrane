
#include<cstring>
#include<vector>

#include <LeMonADE/core/Ingredients.h>
#include <LeMonADE/core/ConfigureSystem.h>
#include <LeMonADE/feature/FeatureMoleculesIO.h>
#include <LeMonADE/feature/FeatureMoleculesIO.h>
#include <LeMonADE/utility/DepthIterator.h>
#include <LeMonADE/utility/DepthIteratorPredicates.h>
#include <LeMonADE/utility/TaskManager.h>
#include <LeMonADE/utility/MonomerGroup.h>
#include <LeMonADE/utility/RandomNumberGenerators.h>
#include <LeMonADE/analyzer/AnalyzerWriteBfmFile.h>
#include <LeMonADE/analyzer/AnalyzerRadiusOfGyration.h>
#include <LeMonADE/updater/UpdaterSimpleSimulator.h>
#include <LeMonADE/updater/UpdaterReadBfmFile.h>


#include <LeMonADE-Extensions/utilities/CommandlineParser.h>
#include <LeMonADE-Extensions/updaters/UpdaterBasicSimulator.h>
#include <LeMonADE-Extensions/updaters/UpdaterLinearChainCreator.h>
#include <LeMonADE-Extensions/analyzers/UnmovedMonomersAnalyzer.h>
#include <LeMonADE-Extensions/analyzers/LatticeOccupationAnalyzer.h>

#include <LeMonADE-ImplicitMembrane/AnalyzerProbabilityDensityZ.h>


int main(int argc, char* argv[])
{
	
	//seed the globally available random number generators
	RandomNumberGenerators rng;
	rng.seedAll();
		
	//default arguments and simulation parameters	
	uint64_t maxMcs=100000000;
	uint32_t writeInterval=10000;
	uint64_t equilibration_steps=10000000;
	uint64_t lowerLimitForEvaluation=10000000;
	
	std::string outputFileTrajectory=std::string("config.bfm"); //filename for output
	
	//size of simulation box
	int32_t boxX=64;
	int32_t boxY=64;
	int32_t boxZ=64;
	
	//polymer definitions
	int32_t n_polymers=1;
	std::vector<int32_t> polymerComposition(32,1);

	double interactionEnergy=0.0; //this is the energy per contact in kT
	std::vector<double> hydrophobicity(4,0.0); //hydrophobicities of monomer types
	
	
	//////////////////////////////////////////////////////////////////////
	//parse command line arguments
	try{
		CommandLineParser cmd;
		
		//add possible options for command line
		cmd.addOption("-m",1,"specify number of mcs to be simulated. default is 1e6");
		cmd.addOption("-f",1,"specify output filename for trajectory.\n\tper default the trajectory is not written.\n\tsyntax: -f filename");
		cmd.addOption("-b",3,"specify box dimensions. syntax: -b x y z. default  64 64 64");
		cmd.addOption("-a",1,"specify interval for analysis, i.e. Rg and trajectory if chosen\n");
		cmd.addOption("-e",1,"specify contact energy");
		cmd.addOption("--rcp",7,"add random copolymer(s) to system. syntax: nPolymers type1 n1 type2 n2 type3 n3 (default 1 polymers, type 1)");		
		cmd.addOption("--hyd",4,"adjust relative hydrophobicity of types 1-4. Default 0.0");
		cmd.addOption("-h",0,"display help");
		
		//parse command line options
		cmd.parse(argv+1,argc-1);
		
		if(argc==1 || cmd.getOption("-h")){
			std::cout<<"****** Linear Chain Simulator *************\n\n"
			<<"Usage: SimulateLinearChain [options]\n";
			cmd.displayHelp();
			exit(0);
		}
		
		cmd.getOption("-m",maxMcs);
		cmd.getOption("-f",outputFileTrajectory);
		cmd.getOption("-b",boxX,0);
		cmd.getOption("-b",boxY,1);
		cmd.getOption("-b",boxZ,2);
		cmd.getOption("-a",writeInterval);
		cmd.getOption("-e",interactionEnergy);
		for(size_t n=0;n<4;n++)
			cmd.getOption("--hyd",hydrophobicity[n],n);
		
		if(cmd.getOption("--rcp",n_polymers,0))
		{
			polymerComposition.clear();
			uint32_t lenA,lenB,lenC;
			int32_t typeA,typeB,typeC;
			
			cmd.getOption("--rcp",typeA,1);
			cmd.getOption("--rcp",lenA,2);
			cmd.getOption("--rcp",typeB,3);
			cmd.getOption("--rcp",lenB,4);
			cmd.getOption("--rcp",typeC,5);
			cmd.getOption("--rcp",lenC,6);
			
			uint32_t na=0;uint32_t nb=0; uint32_t nc=0; uint32_t n=0;
			//used later for distributing the charges
			std::vector<int32_t> indicesA,indicesB,indicesC;
			
			while(n<lenA+lenB+lenC){
				uint32_t type=rng.r250_rand32()%(lenA+lenB+lenC);
				
				if(0<=type && type<lenA && na<lenA){
					polymerComposition.push_back(typeA);
					indicesA.push_back(n);
					na++;
					n++;
				
				}
				else if(lenA<=type && type<lenA+lenB && nb<lenB){
					polymerComposition.push_back(typeB);
					indicesB.push_back(n);
					nb++;
					n++;
				
				}
				else if(lenA+lenB<=type && type<lenA+lenB+lenC && nc<lenC){
					polymerComposition.push_back(typeC);
					indicesC.push_back(n);
					nc++;
					n++;
				
				}
				
			}
			
			std::cout<<"random copolymer sequence:";
			for(size_t n=0;n<polymerComposition.size();n++) std::cout<<polymerComposition[n];
			std::cout<<"\n";
			
			
		}
		
		
		
	}
	catch(std::exception& e){
		std::cerr<<"Error while parsing arguments:\n"
		<<e.what()<<std::endl;
	}
	catch(...){
		std::cerr<<"Error while parsing arguments: unknown exception\n";
	}
	
	//////////////////////////////////////////////////////////////////////
	//set up the sytem and simulate
	try{
		
		
		
		
		typedef LOKI_TYPELIST_2(FeatureMoleculesIO,FeatureNNInteraction) Features;
		typedef ConfigureSystem<VectorInt3,Features, 6> Config;
		typedef Ingredients<Config> Ing;
		Ing myIngredients;
		
		myIngredients.setBoxX(boxX);
		myIngredients.setBoxY(boxY);
		myIngredients.setBoxZ(boxZ);
		myIngredients.setPeriodicX(true);
		myIngredients.setPeriodicY(true);
		myIngredients.setPeriodicZ(true);
		myIngredients.modifyBondset().addBFMclassicBondset();
		

		
		
		for(int32_t a=1;a<=4;a++){
			for(int32_t b=1;b<=4;b++){
				myIngredients.setInteraction(a,b,interactionEnergy*(hydrophobicity[a-1]+hydrophobicity[b-1]));
			}	
		}
		
		myIngredients.synchronize(myIngredients);
		
		UpdaterLinearChainCreator<Ing> polymerCreator(myIngredients,polymerComposition.size(),n_polymers);
		polymerCreator.resize(polymerComposition);
		polymerCreator.initialize();
		polymerCreator.execute();
		
		
				
		//this is used later when creating polymers and analyzing them
		std::vector<MonomerGroup<typename Ing::molecules_type> > polymersGroupVector;
		fill_connected_groups(myIngredients.getMolecules(),
				      polymersGroupVector,
				      MonomerGroup<typename Ing::molecules_type>(myIngredients.getMolecules()),
				      alwaysTrue());
		
		
		TaskManager taskManager;
		
		//calculate the intervals for short term analysis of translocation.
		uint64_t short_term_analysis_interval=100;
		int32_t execution_other_analyzers=writeInterval/short_term_analysis_interval;
		int32_t execution_lattice_check=100*execution_other_analyzers;
		
		
		//this is run only once at the beginning for equilibration.
		taskManager.addUpdater(new UpdaterSimpleSimulator<Ing,MoveLocalSc>(myIngredients,
										   equilibration_steps),
				       0);

		taskManager.addUpdater(new UpdaterBasicSimulator<Ing,MoveLocalSc>(myIngredients,
										  short_term_analysis_interval,
										  maxMcs),
				       1);
		
		/*add system consistency checker*/
		//taskManager.addAnalyzer(latticeCheck,execution_lattice_check);
		taskManager.addAnalyzer(new UnmovedMonomersAnalyzer<Ing>(myIngredients),execution_lattice_check);
		taskManager.addAnalyzer(new AnalyzerRadiusOfGyration<Ing>(myIngredients,
									  "polymer"),
					execution_other_analyzers);
		taskManager.addAnalyzer(new AnalyzerProbabilityDensity<Ing>(myIngredients,polymersGroupVector),execution_other_analyzers);
		
		
		taskManager.addAnalyzer(new AnalyzerWriteBfmFile<Ing>(outputFileTrajectory,
								      myIngredients,
								      AnalyzerWriteBfmFile<Ing>::NEWFILE),
					execution_other_analyzers);
		
		/*init and run the simulation*/
		taskManager.initialize();
		taskManager.run();
		taskManager.cleanup();
		
		
		
	}
	catch(std::exception& e){
		std::cerr<<"Error while simulating :\n"
		<<e.what()<<std::endl;
	}
	catch(...){
		std::cerr<<"Error while simulating: unknown exception\n";
	}
	
	return 0;
	
}


#include <LeMonADE-ImplicitMembrane/FeatureImplicitSolventd3_x.h>

FeatureImplicitSolventd3_X::FeatureImplicitSolventd3_X()
{
  for(size_t n=0;n<7;n++){
    for(size_t m=0;m<4;m++){
      interactionEnergySolvent[n][m]=0.0;
    }
    for(size_t m=0;m<7;m++){
      interactionEnergyContact[n][m]=0.0;
    }
    for(size_t m=0;m<256;m++){
      probabilityLookup[n][m]=1.0;
    }
  }

  solvent=0;
}


void FeatureImplicitSolventd3_X::setSolventDistribution(SolventDistribution* pSolventDist)
{
  solvent=pSolventDist;
}


void FeatureImplicitSolventd3_X::setSolventInteraction(uint32_t solventType,uint32_t monoType,double energy)
{
  if(solventType>3)
    throw std::runtime_error("FeatureImplicitSolventd3_X: Tag for implicit solvent must be smaller 4\n");	  

  if(monoType>7)
    throw std::runtime_error("FeatureImplicitSolventd3_X: Tag for monomers must be smaller 16\n");	  

  //only have to set this one value. 
  //if the solvent is depleted, the lattice entry, which is directly used as index
  //in the lookup, is different from solventType. same applies for the case of the
  //lattice site being occupied.
  probabilityLookup[monoType-1][solventType]=std::exp(-energy);
  
  interactionEnergySolvent[monoType-1][solventType]=energy;
  
}

double FeatureImplicitSolventd3_X::getSolventInteraction(uint32_t solventType,uint32_t monoType) const
{
  if(solventType>3)
    throw std::runtime_error("FeatureImplicitSolventd3_X: Tag for implicit solvent must be smaller 4\n");	  

  if(monoType>7)
    throw std::runtime_error("FeatureImplicitSolventd3_X: Tag for monomers must be smaller 16\n");	  

  return interactionEnergySolvent[monoType-1][solventType];
  
}

void FeatureImplicitSolventd3_X::setContactInteraction(uint32_t typeA,uint32_t typeB, double energy)
{
 
  if(typeA>7 || typeB>7)
    throw std::runtime_error("FeatureImplicitSolventd3_X: Tag for monomers must be smaller 16\n");	  

  interactionEnergyContact[typeA-1][typeB-1]=energy;
  interactionEnergyContact[typeB-1][typeA-1]=energy;

  double probability=std::exp(-energy);
  uint32_t indexA=0;
  uint32_t indexB=0;
  
  indexA=(typeA<<5);
  indexB=(typeB<<5);

  //here we set for any first 5 bits the probability
  for(uint32_t flag=0;flag<32;flag++){
	  probabilityLookup[typeA-1][indexB|flag]=probability;
	  probabilityLookup[typeB-1][indexA|flag]=probability;
  }
  
}


double FeatureImplicitSolventd3_X::getContactInteraction(uint32_t typeA, uint32_t typeB) const
{
  return interactionEnergyContact[typeA-1][typeB-1];
}

double FeatureImplicitSolventd3_X::getProbabilityFactor(uint32_t monoType, uint32_t latticeEntry) const
{
#ifdef DEBUG
  if(monoType>7)
    throw std::runtime_error("FeatureImplicitSolventd3_X: attribute tag larger 15 encountered");
#endif
  return probabilityLookup[monoType-1][latticeEntry];
}



// for d=3
const VectorInt3 FeatureImplicitSolventd3_X::interactionShell_plusX_on[44]={
VectorInt3 (4, -1, 2) ,                                                                              
VectorInt3 (4, -2, 0) ,                                                                              
VectorInt3 (4, 2, 2) ,                                                                               
VectorInt3 (4, -1, 0) ,                                                                              
VectorInt3 (4, -1, 3) ,                                                                              
VectorInt3 (4, 3, 2) ,                                                                               
VectorInt3 (4, -2, 2) ,                                                                              
VectorInt3 (4, 0, 3) ,                                                                               
VectorInt3 (4, -1, -1) ,                                                                             
VectorInt3 (4, 0, 2) ,                                                                               
VectorInt3 (2, 4, 1) ,                                                                               
VectorInt3 (4, 3, 0) ,                                                                               
VectorInt3 (3, 3, 3) ,                                                                               
VectorInt3 (2, 4, 0) ,                                                                               
VectorInt3 (4, 3, 1) ,                                                                               
VectorInt3 (2, 1, -3) ,                                                                              
VectorInt3 (4, 1, -1) ,                                                                              
VectorInt3 (4, 1, 2) ,                                                                               
VectorInt3 (5, 1, 1) ,                                                                               
VectorInt3 (4, 0, -1) ,                                                                              
VectorInt3 (2, 0, -3) ,                                                                              
VectorInt3 (4, -2, -1) ,                                                                             
VectorInt3 (5, 1, 0) ,                                                                               
VectorInt3 (2, 0, 4) ,                                                                               
VectorInt3 (2, 1, 4) ,                                                                               
VectorInt3 (4, 1, -2) ,                                                                              
VectorInt3 (4, 2, -2) ,                                                                              
VectorInt3 (5, 0, 1) ,                                                                               
VectorInt3 (4, 0, -2) ,                                                                              
VectorInt3 (4, -2, 1) ,                                                                              
VectorInt3 (4, 3, -1) ,                                                                              
VectorInt3 (4, 2, -1) ,                                                                              
VectorInt3 (5, 0, 0) ,                                                                               
VectorInt3 (4, -1, 1) ,                                                                              
VectorInt3 (2, -3, 0) ,                                                                              
VectorInt3 (4, 1, 3) ,                                                                               
VectorInt3 (2, -3, 1) ,                                                                              
VectorInt3 (3, 3, -2) ,                                                                              
VectorInt3 (4, 2, 1) ,                                                                               
VectorInt3 (4, -1, -2) ,                                                                             
VectorInt3 (3, -2, -2) ,                                                                             
VectorInt3 (4, 2, 0) ,                                                                               
VectorInt3 (4, 2, 3) ,                                                                               
VectorInt3 (3, -2, 3)
};

const VectorInt3 FeatureImplicitSolventd3_X::interactionShell_plusX_off[48]={
VectorInt3 (2, 1, 1) ,                                                                               
VectorInt3 (-3, 0, 0) ,                                                                              
VectorInt3 (-2, 1, -1) ,                                                                             
VectorInt3 (-3, 0, 1) ,                                                                              
VectorInt3 (-2, -2, -1) ,                                                                            
VectorInt3 (-2, 3, 0) ,                                                                              
VectorInt3 (-2, -1, -2) ,                                                                            
VectorInt3 (0, 1, 4) ,                                                                               
VectorInt3 (-2, 2, -1) ,                                                                             
VectorInt3 (-2, 3, 1) ,                                                                              
VectorInt3 (-2, -2, 0) ,                                                                             
VectorInt3 (-2, -1, 3) ,                                                                             
VectorInt3 (-2, 3, 2) ,                                                                              
VectorInt3 (-2, -1, 2) ,                                                                             
VectorInt3 (-1, 3, -2) ,                                                                             
VectorInt3 (-1, 3, 3) ,                                                                              
VectorInt3 (-2, 2, -2) ,                                                                             
VectorInt3 (-2, -2, 2) ,                                                                             
VectorInt3 (0, -3, 0) ,                                                                              
VectorInt3 (-2, 2, 2) ,                                                                              
VectorInt3 (2, 1, 0) ,                                                                               
VectorInt3 (0, -3, 1) ,                                                                              
VectorInt3 (2, 0, 1) ,                                                                               
VectorInt3 (0, 0, 4) ,                                                                               
VectorInt3 (0, 0, -3) ,                                                                              
VectorInt3 (2, 0, 0) ,                                                                               
VectorInt3 (-2, 1, -2) ,                                                                             
VectorInt3 (-2, 0, 3) ,                                                                              
VectorInt3 (0, 4, 1) ,                                                                               
VectorInt3 (-2, 0, -1) ,                                                                             
VectorInt3 (-2, 0, 2) ,                                                                              
VectorInt3 (-2, 0, -2) ,                                                                             
VectorInt3 (-2, 1, 2) ,                                                                              
VectorInt3 (-2, 2, 3) ,                                                                              
VectorInt3 (-2, -1, 0) ,                                                                             
VectorInt3 (-2, 1, 3) ,                                                                              
VectorInt3 (-1, -2, 3) ,                                                                             
VectorInt3 (-3, 1, 0) ,                                                                              
VectorInt3 (-2, 2, 1) ,                                                                              
VectorInt3 (-2, 3, -1) ,                                                                             
VectorInt3 (-2, -1, 1) ,                                                                             
VectorInt3 (-2, 2, 0) ,                                                                              
VectorInt3 (0, 4, 0) ,                                                                               
VectorInt3 (-2, -2, 1) ,                                                                             
VectorInt3 (-3, 1, 1) ,                                                                              
VectorInt3 (0, 1, -3) ,                                                                              
VectorInt3 (-2, -1, -1) ,                                                                            
VectorInt3 (-1, -2, -2)
};

const VectorInt3 FeatureImplicitSolventd3_X::interactionShell_minusX_on[44]={
VectorInt3 (-3, -2, 2) ,                                                                             
VectorInt3 (-3, -1, 2) ,                                                                             
VectorInt3 (-1, -3, 1) ,                                                                             
VectorInt3 (-3, -1, -1) ,                                                                            
VectorInt3 (-3, -1, 3) ,                                                                             
VectorInt3 (-3, 3, 1) ,                                                                              
VectorInt3 (-3, 1, -2) ,                                                                             
VectorInt3 (-2, -2, -2) ,                                                                            
VectorInt3 (-3, -2, -1) ,                                                                             
VectorInt3 (-2, 3, 3) ,                                                                              
VectorInt3 (-3, 1, 3) ,                                                                              
VectorInt3 (-1, 1, -3) ,                                                                             
VectorInt3 (-3, 3, 2) ,                                                                              
VectorInt3 (-3, 0, -2) ,                                                                             
VectorInt3 (-1, 0, -3) ,                                                                             
VectorInt3 (-3, 2, 0) ,                                                                              
VectorInt3 (-3, 2, 1) ,                                                                              
VectorInt3 (-3, 2, -1) ,                                                                             
VectorInt3 (-3, 2, 2) ,                                                                              
VectorInt3 (-4, 1, 0) ,                                                                              
VectorInt3 (-4, 0, 1) ,                                                                              
VectorInt3 (-3, -2, 0) ,                                                                             
VectorInt3 (-3, 2, 3) ,                                                                              
VectorInt3 (-3, -1, 0) ,                                                                             
VectorInt3 (-1, 4, 1) ,                                                                              
VectorInt3 (-4, 0, 0) ,                                                                              
VectorInt3 (-1, 0, 4) ,                                                                              
VectorInt3 (-3, -1, 1) ,                                                                             
VectorInt3 (-1, -3, 0) ,                                                                             
VectorInt3 (-2, -2, 3) ,                                                                             
VectorInt3 (-3, 3, 0) ,     
VectorInt3 (-3, 1, -1) ,                                                                             
VectorInt3 (-2, 3, -2) ,                                                                             
VectorInt3 (-3, -1, -2) ,                                                                            
VectorInt3 (-3, 1, 2) ,                                                                              
VectorInt3 (-3, 0, 3) ,                                                                              
VectorInt3 (-3, 0, -1) ,                                                                             
VectorInt3 (-3, 3, -1) ,                                                                             
VectorInt3 (-3, 0, 2) ,                                                                              
VectorInt3 (-3, 2, -2) ,                                                                             
VectorInt3 (-3, -2, 1) ,                                                                             
VectorInt3 (-1, 1, 4) ,                                                                              
VectorInt3 (-4, 1, 1) ,                                                                              
VectorInt3 (-1, 4, 0)
};

const VectorInt3 FeatureImplicitSolventd3_X::interactionShell_minusX_off[48]={
VectorInt3 (3, 0, 2) ,                                                                               
VectorInt3 (1, 0, -3) ,                                                                              
VectorInt3 (3, -2, 0) ,                                                                              
VectorInt3 (3, 0, 3) ,                                                                               
VectorInt3 (3, -2, 1) ,                                                                              
VectorInt3 (4, 0, 0) ,                                                                               
VectorInt3 (1, 1, -3) ,                                                                              
VectorInt3 (-1, 0, 1) ,                                                                              
VectorInt3 (3, 3, 1) ,                                                                               
VectorInt3 (1, -3, 0) ,                                                                              
VectorInt3 (2, -2, -2) ,                                                                             
VectorInt3 (3, -1, 0) ,                                                                              
VectorInt3 (3, 3, 0) ,                                                                               
VectorInt3 (1, 4, 0) ,                                                                               
VectorInt3 (3, 3, 2) ,                                                                               
VectorInt3 (1, 4, 1) ,                                                                               
VectorInt3 (3, -2, -1) ,                                                                             
VectorInt3 (3, -2, 2) ,                                                                              
VectorInt3 (3, 2, 2) ,                                                                               
VectorInt3 (3, 2, -2) ,                                                                              
VectorInt3 (3, 2, -1) ,                                                                              
VectorInt3 (-1, 0, 0) ,                                                                              
VectorInt3 (3, 1, 2) ,                                                                               
VectorInt3 (3, 2, 0) ,                                                                               
VectorInt3 (-1, 1, 0) ,                                                                              
VectorInt3 (3, -1, 1) ,                                                                              
VectorInt3 (3, 1, 3) ,                                                                               
VectorInt3 (3, 2, 1) ,                                                                               
VectorInt3 (3, -1, -1) ,                                                                             
VectorInt3 (4, 1, 0) ,                                                                               
VectorInt3 (1, -3, 1) ,                                                                              
VectorInt3 (2, -2, 3) ,                                                                              
VectorInt3 (3, 1, -1) ,                                                                              
VectorInt3 (1, 0, 4) ,                                                                               
VectorInt3 (3, 2, 3) ,                                                                               
VectorInt3 (3, 0, -1) ,                                                                              
VectorInt3 (2, 3, -2) ,                                                                              
VectorInt3 (3, 0, -2) ,                                                                              
VectorInt3 (-1, 1, 1) ,                                                                              
VectorInt3 (3, 3, -1) ,                                                                              
VectorInt3 (3, -1, 3) ,                                                                              
VectorInt3 (3, 1, -2) ,                                                                              
VectorInt3 (3, -1, -2) ,                                                                             
VectorInt3 (4, 0, 1) ,                                                                               
VectorInt3 (2, 3, 3) ,                                                                               
VectorInt3 (3, -1, 2) ,                                                                              
VectorInt3 (1, 1, 4) ,                                                                               
VectorInt3 (4, 1, 1)
};

const VectorInt3 FeatureImplicitSolventd3_X::interactionShell_plusY_on[44]={
VectorInt3 (1, 4, 3) ,                                                                               
VectorInt3 (0, 5, 0) ,                                                                               
VectorInt3 (-2, 4, 0) ,                                                                              
VectorInt3 (0, 4, -2) ,                                                                              
VectorInt3 (-1, 4, -2) ,                                                                             
VectorInt3 (3, 4, 0) ,                                                                               
VectorInt3 (3, 4, 1) ,                                                                               
VectorInt3 (1, 4, 2) ,                                                                               
VectorInt3 (2, 4, 1) ,                                                                               
VectorInt3 (2, 4, 2) ,                                                                               
VectorInt3 (3, 4, 2) ,                                                                               
VectorInt3 (0, 5, 1) ,                                                                               
VectorInt3 (0, 4, -1) ,                                                                              
VectorInt3 (3, 3, 3) ,                                                                               
VectorInt3 (2, 4, 0) ,                                                                               
VectorInt3 (-2, 3, 3) ,                                                                              
VectorInt3 (2, 4, 3) ,                                                                               
VectorInt3 (-1, 4, -1) ,                                                                             
VectorInt3 (1, 2, -3) ,                                                                              
VectorInt3 (-3, 2, 0) ,                                                                              
VectorInt3 (2, 4, -1) ,                                                                              
VectorInt3 (-3, 2, 1) ,                                                                              
VectorInt3 (-1, 4, 1) ,                                                                              
VectorInt3 (1, 4, -1) ,                                                                              
VectorInt3 (2, 4, -2) ,                                                                              
VectorInt3 (0, 2, -3) ,                                                                              
VectorInt3 (1, 2, 4) ,                                                                               
VectorInt3 (-2, 3, -2) ,                                                                             
VectorInt3 (0, 4, 3) ,                                                                               
VectorInt3 (-1, 4, 3) ,                                                                              
VectorInt3 (-2, 4, -1) ,                                                                             
VectorInt3 (0, 4, 2) ,                                                                               
VectorInt3 (-1, 4, 2) ,                                                                              
VectorInt3 (-2, 4, 2) ,                                                                              
VectorInt3 (3, 4, -1) ,                                                                              
VectorInt3 (3, 3, -2) ,                                                                              
VectorInt3 (-2, 4, 1) ,                                                                              
VectorInt3 (1, 4, -2) ,                                                                              
VectorInt3 (4, 2, 1) ,                                                                               
VectorInt3 (0, 2, 4) ,                                                                               
VectorInt3 (4, 2, 0) ,                                                                               
VectorInt3 (1, 5, 1) ,                                                                               
VectorInt3 (1, 5, 0) ,                                                                               
VectorInt3 (-1, 4, 0)
};

const VectorInt3 FeatureImplicitSolventd3_X::interactionShell_plusY_off[48]={
VectorInt3 (1, -2, 3) ,                                                                              
VectorInt3 (-3, 0, 0) ,                                                                              
VectorInt3 (1, -2, -1) ,                                                                             
VectorInt3 (3, -2, 1) ,                                                                              
VectorInt3 (-1, -2, -1) ,                                                                            
VectorInt3 (1, -3, 1) ,                                                                              
VectorInt3 (1, 0, -3) ,                                                                              
VectorInt3 (1, -3, 0) ,                                                                              
VectorInt3 (2, -2, -2) ,                                                                             
VectorInt3 (3, -2, 0) ,                                                                              
VectorInt3 (0, 2, 1) ,                                                                               
VectorInt3 (-1, -2, 0) ,                                                                             
VectorInt3 (-2, -1, 3) ,                                                                             
VectorInt3 (4, 0, 1) ,                                                                               
VectorInt3 (0, 2, 0) ,                                                                               
VectorInt3 (1, 0, 4) ,                                                                               
VectorInt3 (4, 0, 0) ,                                                                               
VectorInt3 (2, -2, -1) ,                                                                             
VectorInt3 (3, -2, -1) ,                                                                             
VectorInt3 (0, -3, 0) ,                                                                              
VectorInt3 (0, -2, -1) ,                                                                             
VectorInt3 (1, -2, -2) ,                                                                             
VectorInt3 (0, -3, 1) ,                                                                              
VectorInt3 (0, 0, 4) ,                                                                               
VectorInt3 (0, 0, -3) ,                                                                              
VectorInt3 (0, -2, -2) ,                                                                             
VectorInt3 (1, 2, 0) ,                                                                               
VectorInt3 (1, 2, 1) ,                                                                               
VectorInt3 (-3, 0, 1) ,                                                                              
VectorInt3 (-2, -2, 2) ,                                                                             
VectorInt3 (3, -1, 3) ,                                                                              
VectorInt3 (2, -2, 3) ,                                                                              
VectorInt3 (-1, -2, 3) ,                                                                             
VectorInt3 (0, -2, 3) ,                                                                              
VectorInt3 (2, -2, 2) ,                                                                              
VectorInt3 (-1, -2, 2) ,                                                                             
VectorInt3 (0, -2, 2) ,                                                                              
VectorInt3 (2, -2, 1) ,                                                                              
VectorInt3 (-1, -2, 1) ,                                                                             
VectorInt3 (2, -2, 0) ,                                                                              
VectorInt3 (-2, -2, 0) ,                                                                             
VectorInt3 (-1, -2, -2) ,                                                                            
VectorInt3 (-2, -2, 1) ,                                                                             
VectorInt3 (3, -1, -2) ,                                                                             
VectorInt3 (3, -2, 2) ,                                                                              
VectorInt3 (1, -2, 2) ,                                                                              
VectorInt3 (-2, -2, -1) ,                                                                            
VectorInt3 (-2, -1, -2)
};

const VectorInt3 FeatureImplicitSolventd3_X::interactionShell_minusY_on[44]={
VectorInt3 (0, -4, 1) ,                                                                              
VectorInt3 (3, -3, 2) ,                                                                              
VectorInt3 (-1, -3, 1) ,                                                                             
VectorInt3 (-2, -3, -1) ,                                                                            
VectorInt3 (2, -3, -2) ,                                                                             
VectorInt3 (1, -1, -3) ,                                                                             
VectorInt3 (3, -3, 1) ,                                                                              
VectorInt3 (3, -3, 0) ,                                                                              
VectorInt3 (-1, -3, -2) ,                                                                            
VectorInt3 (0, -3, 3) ,                                                                              
VectorInt3 (-2, -2, -2) ,                                                                            
VectorInt3 (2, -3, -1) ,                                                                             
VectorInt3 (2, -3, 3) ,                                                                              
VectorInt3 (0, -1, -3) ,                                                                             
VectorInt3 (1, -3, -1) ,                                                                             
VectorInt3 (1, -3, 3) ,                                                                              
VectorInt3 (1, -3, 2) ,                                                                              
VectorInt3 (-1, -3, 0) ,                                                                             
VectorInt3 (-2, -3, 2) ,                                                                             
VectorInt3 (0, -4, 0) ,                                                                              
VectorInt3 (-3, -1, 0) ,                                                                             
VectorInt3 (0, -3, -2) ,                                                                             
VectorInt3 (0, -1, 4) ,                                                                              
VectorInt3 (0, -3, 2) ,                                                                              
VectorInt3 (1, -4, 1) ,                                                                              
VectorInt3 (0, -3, -1) ,                                                                             
VectorInt3 (2, -3, 2) ,                                                                              
VectorInt3 (1, -1, 4) ,                                                                              
VectorInt3 (-1, -3, 2) ,                                                                             
VectorInt3 (4, -1, 1) ,                                                                              
VectorInt3 (-2, -2, 3) ,                                                                             
VectorInt3 (-1, -3, 3) ,                                                                             
VectorInt3 (2, -3, 0) ,                                                                              
VectorInt3 (-2, -3, 0) ,                                                                             
VectorInt3 (2, -3, 1) ,                                                                              
VectorInt3 (1, -4, 0) ,                                                                              
VectorInt3 (-2, -3, 1) ,                                                                             
VectorInt3 (-1, -3, -1) ,                                                                            
VectorInt3 (1, -3, -2) ,                                                                             
VectorInt3 (3, -2, -2) ,                                                                             
VectorInt3 (4, -1, 0) ,                                                                              
VectorInt3 (3, -3, -1) ,                                                                             
VectorInt3 (-3, -1, 1) ,                                                                               
VectorInt3 (3, -2, 3)
};

const VectorInt3 FeatureImplicitSolventd3_X::interactionShell_minusY_off[48]={
VectorInt3 (2, 3, -1) ,                                                                              
VectorInt3 (2, 3, 3) ,                                                                               
VectorInt3 (-3, 1, 1) ,                                                                              
VectorInt3 (3, 3, 1) ,                                                                               
VectorInt3 (0, 3, 3) ,                                                                               
VectorInt3 (1, 1, -3) ,                                                                              
VectorInt3 (-2, 3, 0) ,                                                                              
VectorInt3 (0, 3, -1) ,                                                                              
VectorInt3 (0, 1, 4) ,                                                                               
VectorInt3 (-1, 3, 1) ,                                                                              
VectorInt3 (-2, 3, 1) ,                                                                              
VectorInt3 (2, 3, 1) ,                                                                               
VectorInt3 (1, 3, 2) ,                                                                               
VectorInt3 (-1, 3, 2) ,                                                                              
VectorInt3 (-1, 3, 3) ,                                                                              
VectorInt3 (2, 3, 2) ,                                                                               
VectorInt3 (-1, 3, -2) ,                                                                             
VectorInt3 (1, 3, -1) ,                                                                              
VectorInt3 (1, 4, 1) ,                                                                               
VectorInt3 (0, 3, -2) ,                                                                              
VectorInt3 (-1, 3, -1) ,                                                                             
VectorInt3 (3, 2, -2) ,                                                                              
VectorInt3 (-2, 2, -2) ,                                                                             
VectorInt3 (3, 2, 3) ,                                                                               
VectorInt3 (0, 3, 2) ,                                                                               
VectorInt3 (3, 3, 0) ,                                                                               
VectorInt3 (1, -1, 1) ,                                                                              
VectorInt3 (-2, 3, 2) ,                                                                              
VectorInt3 (0, -1, 1) ,                                                                              
VectorInt3 (0, -1, 0) ,                                                                              
VectorInt3 (-2, 2, 3) ,                                                                              
VectorInt3 (1, 4, 0) ,                                                                               
VectorInt3 (4, 1, 0) ,                                                                               
VectorInt3 (-3, 1, 0) ,                                                                              
VectorInt3 (2, 3, -2) ,                                                                              
VectorInt3 (2, 3, 0) ,                                                                               
VectorInt3 (3, 3, 2) ,                                                                               
VectorInt3 (-2, 3, -1) ,                                                                             
VectorInt3 (0, 4, 1) ,                                                                               
VectorInt3 (3, 3, -1) ,                                                                              
VectorInt3 (-1, 3, 0) ,                                                                              
VectorInt3 (1, -1, 0) ,                                                                              
VectorInt3 (0, 4, 0) ,                                                                               
VectorInt3 (1, 3, -2) ,                                                                              
VectorInt3 (0, 1, -3) ,                                                                              
VectorInt3 (1, 1, 4) ,                                                                               
VectorInt3 (1, 3, 3) ,                                                                               
VectorInt3 (4, 1, 1)
};

const VectorInt3 FeatureImplicitSolventd3_X::interactionShell_plusZ_on[44]={                                                                    
VectorInt3 (2, 2, 4) ,                                                                               
VectorInt3 (3, 0, 4) ,                                                                               
VectorInt3 (-2, 2, 4) ,                                                                              
VectorInt3 (-1, -2, 4) ,                                                                             
VectorInt3 (1, 4, 2) ,                                                                               
VectorInt3 (4, 0, 2) ,                                                                               
VectorInt3 (0, 1, 5) ,                                                                               
VectorInt3 (3, 3, 3) ,                                                                               
VectorInt3 (2, 0, 4) ,                                                                               
VectorInt3 (1, -3, 2) ,                                                                              
VectorInt3 (-2, 3, 3) ,                                                                              
VectorInt3 (4, 1, 2) ,                                                                               
VectorInt3 (1, 0, 5) ,                                                                               
VectorInt3 (-1, 3, 4) ,                                                                              
VectorInt3 (3, 1, 4) ,                                                                               
VectorInt3 (0, 0, 5) ,                                                                               
VectorInt3 (0, -1, 4) ,                                                                              
VectorInt3 (0, -3, 2) ,                                                                              
VectorInt3 (0, 2, 4) ,                                                                               
VectorInt3 (-2, -1, 4) ,                                                                             
VectorInt3 (-1, -1, 4) ,                                                                             
VectorInt3 (1, 1, 5) ,                                                                               
VectorInt3 (2, 1, 4) ,                                                                               
VectorInt3 (2, -1, 4) ,                                                                              
VectorInt3 (2, -2, 4) ,                                                                              
VectorInt3 (1, -2, 4) ,                                                                              
VectorInt3 (-2, 0, 4) ,                                                                              
VectorInt3 (1, 2, 4) ,                                                                               
VectorInt3 (0, -2, 4) ,                                                                              
VectorInt3 (-2, -2, 3) ,                                                                             
VectorInt3 (-1, 2, 4) ,                                                                              
VectorInt3 (3, 2, 4) ,                                                                               
VectorInt3 (0, 4, 2) ,                                                                               
VectorInt3 (0, 3, 4) ,                                                                               
VectorInt3 (3, -1, 4) ,                                                                              
VectorInt3 (-2, 1, 4) ,                                                                              
VectorInt3 (-3, 1, 2) ,                                                                              
VectorInt3 (1, -1, 4) ,                                                                              
VectorInt3 (-3, 0, 2) ,                                                                              
VectorInt3 (1, 3, 4) ,                                                                               
VectorInt3 (2, 3, 4) ,                                                                               
VectorInt3 (-1, 1, 4) ,                                                                              
VectorInt3 (3, -2, 3) ,                                                                              
VectorInt3 (-1, 0, 4) 
};

const VectorInt3 FeatureImplicitSolventd3_X::interactionShell_plusZ_off[48]={
VectorInt3 (1, 0, -3) ,                                                                              
VectorInt3 (3, 0, -2) ,                                                                              
VectorInt3 (-2, 1, -2) ,                                                                             
VectorInt3 (0, 1, 2) ,                                                                               
VectorInt3 (-2, -2, -1) ,                                                                            
VectorInt3 (1, 1, -3) ,                                                                              
VectorInt3 (2, 1, -2) ,                                                                              
VectorInt3 (1, -2, -2) ,                                                                             
VectorInt3 (-1, -1, -2) ,                                                                            
VectorInt3 (-1, 1, -2) ,                                                                             
VectorInt3 (-1, 2, -2) ,                                                                             
VectorInt3 (2, -2, -2) ,                                                                             
VectorInt3 (1, 1, 2) ,                                                                               
VectorInt3 (1, 4, 0) ,                                                                               
VectorInt3 (4, 0, 0) ,                                                                               
VectorInt3 (-2, 2, -2) ,                                                                             
VectorInt3 (0, 3, -2) ,                                                                              
VectorInt3 (3, -2, -1) ,                                                                             
VectorInt3 (0, -3, 0) ,                                                                              
VectorInt3 (0, -2, -2) ,                                                                             
VectorInt3 (1, -1, -2) ,                                                                             
VectorInt3 (3, 2, -2) ,                                                                              
VectorInt3 (0, 0, -3) ,                                                                              
VectorInt3 (3, 1, -2) ,                                                                              
VectorInt3 (1, 2, -2) ,                                                                              
VectorInt3 (2, 2, -2) ,                                                                              
VectorInt3 (-1, 0, -2) ,                                                                             
VectorInt3 (0, 0, 2) ,                                                                               
VectorInt3 (2, 0, -2) ,                                                                              
VectorInt3 (-2, 0, -2) ,                                                                             
VectorInt3 (4, 1, 0) ,                                                                               
VectorInt3 (-1, 3, -2) ,                                                                             
VectorInt3 (-3, 1, 0) ,                                                                              
VectorInt3 (2, 3, -2) ,                                                                              
VectorInt3 (0, -1, -2) ,                                                                             
VectorInt3 (0, 2, -2) ,                                                                              
VectorInt3 (-2, 3, -1) ,                                                                             
VectorInt3 (-3, 0, 0) ,                                                                              
VectorInt3 (2, -1, -2) ,                                                                             
VectorInt3 (3, 3, -1) ,                                                                              
VectorInt3 (0, 4, 0) ,                                                                               
VectorInt3 (-2, -1, -2) ,                                                                            
VectorInt3 (1, 3, -2) ,                                                                              
VectorInt3 (3, -1, -2) ,                                                                             
VectorInt3 (0, 1, -3) ,                                                                              
VectorInt3 (1, 0, 2) ,                                                                               
VectorInt3 (-1, -2, -2) ,                                                                            
VectorInt3 (1, -3, 0) 
};

const VectorInt3 FeatureImplicitSolventd3_X::interactionShell_minusZ_on[44]={
VectorInt3 (1, -2, -3) ,
VectorInt3 (2, -1, -3) ,
VectorInt3 (3, -1, -3) ,
VectorInt3 (0, 4, -1) ,
VectorInt3 (1, -1, -3) ,
VectorInt3 (-2, -1, -3) ,
VectorInt3 (1, 1, -4) ,
VectorInt3 (0, -1, -3) ,
VectorInt3 (2, 2, -3) ,
VectorInt3 (0, -2, -3) ,
VectorInt3 (2, -2, -3) ,
VectorInt3 (-1, -1, -3) ,
VectorInt3 (2, 1, -3) ,
VectorInt3 (4, 1, -1) ,
VectorInt3 (-1, -2, -3) ,
VectorInt3 (-2, 1, -3) ,
VectorInt3 (-3, 0, -1) ,
VectorInt3 (4, 0, -1) ,
VectorInt3 (2, 0, -3) ,
VectorInt3 (1, 2, -3) ,
VectorInt3 (-2, 0, -3) ,
VectorInt3 (0, 0, -4) ,
VectorInt3 (3, 1, -3) ,
VectorInt3 (-2, 2, -3) ,
VectorInt3 (0, 3, -3) ,
VectorInt3 (-1, 0, -3) ,
VectorInt3 (0, -3, -1) ,
VectorInt3 (3, 2, -3) ,
VectorInt3 (-1, 2, -3) ,
VectorInt3 (0, 2, -3) ,
VectorInt3 (1, 4, -1) ,
VectorInt3 (-3, 1, -1) ,
VectorInt3 (3, 0, -3) ,
VectorInt3 (2, 3, -3) ,
VectorInt3 (-1, 3, -3) ,
VectorInt3 (-2, 3, -2) ,
VectorInt3 (3, 3, -2) ,
VectorInt3 (0, 1, -4) ,
VectorInt3 (1, -3, -1) ,
VectorInt3 (3, -2, -2) ,
VectorInt3 (1, 3, -3) ,
VectorInt3 (1, 0, -4) ,
VectorInt3 (-1, 1, -3) ,
VectorInt3 (-2, -2, -2)
};

const VectorInt3 FeatureImplicitSolventd3_X::interactionShell_minusZ_off[48]={
VectorInt3 (-1, 0, 3) ,                                                                              
VectorInt3 (1, -2, 3) ,                                                                              
VectorInt3 (-2, 2, 3) ,                                                                              
VectorInt3 (3, 0, 3) ,                                                                               
VectorInt3 (2, 2, 3) ,                                                                               
VectorInt3 (-3, 0, 1) ,                                                                              
VectorInt3 (1, -3, 1) ,                                                                              
VectorInt3 (1, -1, 3) ,                                                                              
VectorInt3 (0, 1, 4) ,                                                                               
VectorInt3 (-2, 1, 3) ,                                                                              
VectorInt3 (-2, -1, 3) ,                                                                             
VectorInt3 (-1, -2, 3) ,                                                                             
VectorInt3 (-2, 3, 2) ,                                                                              
VectorInt3 (1, 1, -1) ,                                                                              
VectorInt3 (1, 0, 4) ,                                                                               
VectorInt3 (-1, 3, 3) ,                                                                              
VectorInt3 (0, 2, 3) ,                                                                               
VectorInt3 (0, -2, 3) ,                                                                              
VectorInt3 (1, 4, 1) ,                                                                               
VectorInt3 (-1, 2, 3) ,                                                                              
VectorInt3 (0, 0, -1) ,                                                                              
VectorInt3 (0, -3, 1) ,                                                                              
VectorInt3 (3, 1, 3) ,                                                                               
VectorInt3 (0, 0, 4) ,                                                                               
VectorInt3 (3, 2, 3) ,                                                                               
VectorInt3 (-2, 0, 3) ,                                                                              
VectorInt3 (0, 3, 3) ,                                                                               
VectorInt3 (2, 0, 3) ,                                                                               
VectorInt3 (1, 2, 3) ,                                                                               
VectorInt3 (4, 1, 1) ,                                                                               
VectorInt3 (2, -1, 3) ,                                                                              
VectorInt3 (-1, 1, 3) ,                                                                              
VectorInt3 (-1, -1, 3) ,                                                                             
VectorInt3 (0, -1, 3) ,                                                                              
VectorInt3 (2, 1, 3) ,                                                                               
VectorInt3 (-2, -2, 2) ,                                                                             
VectorInt3 (0, 4, 1) ,                                                                               
VectorInt3 (2, -2, 3) ,                                                                              
VectorInt3 (3, 3, 2) ,                                                                               
VectorInt3 (1, 0, -1) ,                                                                              
VectorInt3 (3, -1, 3) ,                                                                              
VectorInt3 (4, 0, 1) ,                                                                               
VectorInt3 (-3, 1, 1) ,                                                                              
VectorInt3 (2, 3, 3) ,                                                                               
VectorInt3 (3, -2, 2) ,                                                                              
VectorInt3 (0, 1, -1) ,                                                                              
VectorInt3 (1, 1, 4) ,
VectorInt3 (1, 3, 3)
};
#include <LeMonADE-ImplicitMembrane/FeatureImplicitSolventd2.h>

FeatureImplicitSolventd2::FeatureImplicitSolventd2()
{
  for(size_t n=0;n<15;n++){
    for(size_t m=0;m<4;m++){
      interactionEnergySolvent[n][m]=0.0;
    }
    for(size_t m=0;m<15;m++){
      interactionEnergyContact[n][m]=0.0;
    }
    for(size_t m=0;m<63;m++){
      probabilityLookup[n][m]=1.0;
    }
  }

  solvent=0;
}


void FeatureImplicitSolventd2::setSolventDistribution(SolventDistribution* pSolventDist)
{
  solvent=pSolventDist;
}


void FeatureImplicitSolventd2::setSolventInteraction(uint32_t solventType,uint32_t monoType,double energy)
{
  if(solventType>3)
    throw std::runtime_error("FeatureImplicitSolventd2: Tag for implicit solvent must be smaller 4\n");	  

  if(monoType>15)
    throw std::runtime_error("FeatureImplicitSolventd2: Tag for monomers must be smaller 16\n");	  

  probabilityLookup[monoType][solventType]=std::exp(-energy);
  interactionEnergySolvent[monoType][solventType]=energy;
  
}

double FeatureImplicitSolventd2::getSolventInteraction(uint32_t solventType,uint32_t monoType) const
{
  if(solventType>3)
    throw std::runtime_error("FeatureImplicitSolventd2: Tag for implicit solvent must be smaller 4\n");	  

  if(monoType>15)
    throw std::runtime_error("FeatureImplicitSolventd2: Tag for monomers must be smaller 16\n");	  

  return interactionEnergySolvent[monoType][solventType];
  
}

void FeatureImplicitSolventd2::setContactInteraction(uint32_t typeA,uint32_t typeB, double energy)
{
 
  if(typeA>15 || typeB>15)
    throw std::runtime_error("FeatureImplicitSolventd2: Tag for monomers must be smaller 16\n");	  

  interactionEnergyContact[typeA][typeB]=energy;
  interactionEnergyContact[typeB][typeA]=energy;

  double probability=std::exp(-energy);
  uint32_t indexA=0;
  uint32_t indexB=0;
  
  indexA=(typeA<<2);
  indexB=(typeB<<2);

  probabilityLookup[typeA][indexB]=probability;
  probabilityLookup[typeA][(indexB|1)]=probability;
  probabilityLookup[typeA][(indexB|2)]=probability;
  probabilityLookup[typeA][(indexB|3)]=probability;

  probabilityLookup[typeB][indexA]=probability;
  probabilityLookup[typeB][(indexA|1)]=probability;
  probabilityLookup[typeB][(indexA|2)]=probability;
  probabilityLookup[typeB][(indexA|3)]=probability;

}


double FeatureImplicitSolventd2::getContactInteraction(uint32_t typeA, uint32_t typeB) const
{
  return interactionEnergyContact[typeA][typeB];
}

double FeatureImplicitSolventd2::getProbabilityFactor(uint32_t monoType, uint32_t latticeEntry) const
{
#ifdef DEBUG
  if(monoType>15)
    throw std::runtime_error("FeatureImplicitSolventd2: attribute tag larger 15 encountered");
#endif
  return probabilityLookup[monoType][latticeEntry];
}

const VectorInt3 FeatureImplicitSolventd2::interactionShell_plusX_on[24]={
VectorInt3 (3, 0, 2) ,
VectorInt3 (3, 0, -1) ,
VectorInt3 (3, -1, 1) ,
VectorInt3 (2, 1, -2) ,
VectorInt3 (2, 3, 0) ,
VectorInt3 (3, -1, 0) ,
VectorInt3 (2, 3, 1) ,
VectorInt3 (4, 0, 1) ,
VectorInt3 (4, 0, 0) ,
VectorInt3 (2, 0, -2) ,
VectorInt3 (3, 2, 2) ,
VectorInt3 (3, 2, -1) ,
VectorInt3 (3, 1, -1) ,
VectorInt3 (3, 1, 2) ,
VectorInt3 (3, 2, 0) ,
VectorInt3 (2, 0, 3) ,
VectorInt3 (3, 2, 1) ,
VectorInt3 (4, 1, 0) ,
VectorInt3 (4, 1, 1) ,
VectorInt3 (2, 1, 3) ,
VectorInt3 (2, -2, 1) ,
VectorInt3 (2, -2, 0) ,
VectorInt3 (3, -1, -1) ,
VectorInt3 (3, -1, 2)
};

const VectorInt3 FeatureImplicitSolventd2::interactionShell_plusX_off[28]={
VectorInt3 (2, 1, 1) ,
VectorInt3 (0, 1, 3) ,
VectorInt3 (-1, -1, -1) ,
VectorInt3 (-1, 1, -1) ,
VectorInt3 (-1, 2, -1) ,
VectorInt3 (0, 0, -2) ,
VectorInt3 (-2, 0, 1) ,
VectorInt3 (2, 0, 1) ,
VectorInt3 (-2, 0, 0) ,
VectorInt3 (2, 0, 0) ,
VectorInt3 (0, 0, 3) ,
VectorInt3 (-1, 0, 2) ,
VectorInt3 (0, 3, 0) ,
VectorInt3 (-1, 0, -1) ,
VectorInt3 (0, -2, 1) ,
VectorInt3 (0, 3, 1) ,
VectorInt3 (-1, 1, 2) ,
VectorInt3 (0, -2, 0) ,
VectorInt3 (2, 1, 0) ,
VectorInt3 (-1, 2, 2) ,
VectorInt3 (-2, 1, 0) ,
VectorInt3 (-1, -1, 2) ,
VectorInt3 (-1, 2, 1) ,
VectorInt3 (-2, 1, 1) ,
VectorInt3 (-1, -1, 1) ,
VectorInt3 (-1, 2, 0) ,
VectorInt3 (-1, -1, 0) ,
VectorInt3 (0, 1, -2) 
};

const VectorInt3 FeatureImplicitSolventd2::interactionShell_minusX_on[24]={
VectorInt3 (-3, 0, 0) ,
VectorInt3 (-1, 1, -2) ,
VectorInt3 (-3, 0, 1) ,
VectorInt3 (-1, 3, 0) ,
VectorInt3 (-2, -1, -1) ,
VectorInt3 (-2, 1, -1) ,
VectorInt3 (-1, 3, 1) ,
VectorInt3 (-2, 2, -1) ,
VectorInt3 (-1, -2, 0) ,
VectorInt3 (-2, -1, 2) ,
VectorInt3 (-3, 1, 0) ,
VectorInt3 (-1, 0, 3) ,
VectorInt3 (-2, 0, -1) ,
VectorInt3 (-2, 0, 2) ,
VectorInt3 (-1, 0, -2) ,
VectorInt3 (-2, 1, 2) ,
VectorInt3 (-3, 1, 1) ,
VectorInt3 (-1, 1, 3) ,
VectorInt3 (-2, 2, 2) ,
VectorInt3 (-2, 2, 1) ,
VectorInt3 (-2, -1, 1) ,
VectorInt3 (-2, 2, 0) ,
VectorInt3 (-2, -1, 0) ,
VectorInt3 (-1, -2, 1)
};

const VectorInt3 FeatureImplicitSolventd2::interactionShell_minusX_off[28]={
VectorInt3 (1, 0, 3) ,
VectorInt3 (1, 3, 1) ,
VectorInt3 (2, 1, -1) ,
VectorInt3 (1, 3, 0) ,
VectorInt3 (1, 0, -2) ,
VectorInt3 (2, -1, -1) ,
VectorInt3 (1, 1, -2) ,
VectorInt3 (-1, 0, 1) ,
VectorInt3 (-1, 0, 0) ,
VectorInt3 (3, 1, 0) ,
VectorInt3 (3, 1, 1) ,
VectorInt3 (2, 0, -1) ,
VectorInt3 (2, 0, 2) ,
VectorInt3 (2, 1, 2) ,
VectorInt3 (2, -1, 2) ,
VectorInt3 (-1, 1, 0) ,
VectorInt3 (2, 2, 2) ,
VectorInt3 (2, -1, 1) ,
VectorInt3 (-1, 1, 1) ,
VectorInt3 (2, 2, 1) ,
VectorInt3 (2, -1, 0) ,
VectorInt3 (1, -2, 0) ,
VectorInt3 (2, 2, 0) ,
VectorInt3 (1, 1, 3) ,
VectorInt3 (3, 0, 0) ,
VectorInt3 (1, -2, 1) ,
VectorInt3 (3, 0, 1) ,
VectorInt3 (2, 2, -1)
};

const VectorInt3 FeatureImplicitSolventd2::interactionShell_plusY_on[24]={
VectorInt3 (1, 3, 2) ,
VectorInt3 (-1, 3, 0) ,
VectorInt3 (2, 3, 0) ,
VectorInt3 (-1, 3, 1) ,
VectorInt3 (2, 3, 1) ,
VectorInt3 (-1, 3, 2) ,
VectorInt3 (1, 4, 0) ,
VectorInt3 (0, 2, 3) ,
VectorInt3 (1, 4, 1) ,
VectorInt3 (0, 3, -1) ,
VectorInt3 (0, 3, 2) ,
VectorInt3 (1, 2, -2) ,
VectorInt3 (3, 2, 0) ,
VectorInt3 (3, 2, 1) ,
VectorInt3 (1, 2, 3) ,
VectorInt3 (-1, 3, -1) ,
VectorInt3 (2, 3, -1) ,
VectorInt3 (0, 2, -2) ,
VectorInt3 (-2, 2, 1) ,
VectorInt3 (2, 3, 2) ,
VectorInt3 (0, 4, 1) ,
VectorInt3 (-2, 2, 0) ,
VectorInt3 (0, 4, 0) ,
VectorInt3 (1, 3, -1)
};

const VectorInt3 FeatureImplicitSolventd2::interactionShell_plusY_off[28]={
VectorInt3 (1, 0, 3) ,
VectorInt3 (1, -1, -1) ,
VectorInt3 (1, -2, 0) ,
VectorInt3 (2, -1, -1) ,
VectorInt3 (0, 2, 1) ,
VectorInt3 (0, 2, 0) ,
VectorInt3 (0, -1, -1) ,
VectorInt3 (0, 0, -2) ,
VectorInt3 (-2, 0, 1) ,
VectorInt3 (-2, 0, 0) ,
VectorInt3 (1, 2, 0) ,
VectorInt3 (0, 0, 3) ,
VectorInt3 (1, 2, 1) ,
VectorInt3 (0, -2, 1) ,
VectorInt3 (0, -2, 0) ,
VectorInt3 (2, -1, 2) ,
VectorInt3 (-1, -1, 2) ,
VectorInt3 (0, -1, 2) ,
VectorInt3 (2, -1, 1) ,
VectorInt3 (-1, -1, 1) ,
VectorInt3 (2, -1, 0) ,
VectorInt3 (1, 0, -2) ,
VectorInt3 (-1, -1, 0) ,
VectorInt3 (3, 0, 0) ,
VectorInt3 (1, -2, 1) ,
VectorInt3 (3, 0, 1) ,
VectorInt3 (1, -1, 2) ,
VectorInt3 (-1, -1, -1)
};

const VectorInt3 FeatureImplicitSolventd2::interactionShell_minusY_on[24]={
VectorInt3 (1, -1, 3) ,
VectorInt3 (3, -1, 0) ,
VectorInt3 (1, -1, -2) ,
VectorInt3 (3, -1, 1) ,
VectorInt3 (1, -3, 1) ,
VectorInt3 (-1, -2, -1) ,
VectorInt3 (2, -2, -1) ,
VectorInt3 (-2, -1, 0) ,
VectorInt3 (0, -3, 0) ,
VectorInt3 (0, -2, -1) ,
VectorInt3 (1, -2, -1) ,
VectorInt3 (0, -3, 1) ,
VectorInt3 (0, -1, -2) ,
VectorInt3 (0, -1, 3) ,
VectorInt3 (2, -2, 2) ,
VectorInt3 (-1, -2, 2) ,
VectorInt3 (0, -2, 2) ,
VectorInt3 (2, -2, 1) ,
VectorInt3 (-2, -1, 1) ,
VectorInt3 (2, -2, 0) ,
VectorInt3 (-1, -2, 0) ,
VectorInt3 (-1, -2, 1) ,
VectorInt3 (1, -2, 2) ,
VectorInt3 (1, -3, 0)
};

const VectorInt3 FeatureImplicitSolventd2::interactionShell_minusY_off[28]={
VectorInt3 (1, 3, 1) ,
VectorInt3 (0, 1, 3) ,
VectorInt3 (1, 3, 0) ,
VectorInt3 (2, 2, -1) ,
VectorInt3 (-1, 2, -1) ,
VectorInt3 (1, 1, -2) ,
VectorInt3 (0, 2, 2) ,
VectorInt3 (1, 2, 2) ,
VectorInt3 (1, 2, -1) ,
VectorInt3 (3, 1, 1) ,
VectorInt3 (0, 3, 0) ,
VectorInt3 (3, 1, 0) ,
VectorInt3 (0, -1, 1) ,
VectorInt3 (0, 3, 1) ,
VectorInt3 (0, -1, 0) ,
VectorInt3 (-1, 2, 2) ,
VectorInt3 (-2, 1, 0) ,
VectorInt3 (2, 2, 2) ,
VectorInt3 (0, 2, -1) ,
VectorInt3 (-1, 2, 1) ,
VectorInt3 (-2, 1, 1) ,
VectorInt3 (2, 2, 1) ,
VectorInt3 (-1, 2, 0) ,
VectorInt3 (1, -1, 0) ,
VectorInt3 (2, 2, 0) ,
VectorInt3 (1, 1, 3) ,
VectorInt3 (1, -1, 1) ,
VectorInt3 (0, 1, -2)
};

const VectorInt3 FeatureImplicitSolventd2::interactionShell_plusZ_on[24]={
VectorInt3 (3, 0, 2) ,
VectorInt3 (1, -1, 3) ,
VectorInt3 (1, 3, 2) ,
VectorInt3 (0, 1, 4) ,
VectorInt3 (2, 2, 3) ,
VectorInt3 (1, 0, 4) ,
VectorInt3 (0, 2, 3) ,
VectorInt3 (-1, 2, 3) ,
VectorInt3 (0, 0, 4) ,
VectorInt3 (0, 3, 2) ,
VectorInt3 (3, 1, 2) ,
VectorInt3 (-1, 0, 3) ,
VectorInt3 (2, 0, 3) ,
VectorInt3 (-2, 0, 2) ,
VectorInt3 (1, 2, 3) ,
VectorInt3 (-2, 1, 2) ,
VectorInt3 (2, -1, 3) ,
VectorInt3 (-1, 1, 3) ,
VectorInt3 (-1, -1, 3) ,
VectorInt3 (0, -1, 3) ,
VectorInt3 (2, 1, 3) ,
VectorInt3 (0, -2, 2) ,
VectorInt3 (1, -2, 2) ,
VectorInt3 (1, 1, 4)
};

const VectorInt3 FeatureImplicitSolventd2::interactionShell_plusZ_off[28]={
VectorInt3 (0, 1, 2) ,
VectorInt3 (-1, -1, -1) ,
VectorInt3 (2, 1, -1) ,
VectorInt3 (1, 3, 0) ,
VectorInt3 (1, 0, -2) ,
VectorInt3 (-1, 1, -1) ,
VectorInt3 (-1, 2, -1) ,
VectorInt3 (2, -1, -1) ,
VectorInt3 (1, 1, -2) ,
VectorInt3 (0, -1, -1) ,
VectorInt3 (0, 0, -2) ,
VectorInt3 (-2, 0, 0) ,
VectorInt3 (1, 2, -1) ,
VectorInt3 (3, 1, 0) ,
VectorInt3 (0, 0, 2) ,
VectorInt3 (2, 0, -1) ,
VectorInt3 (0, 3, 0) ,
VectorInt3 (-1, 0, -1) ,
VectorInt3 (0, -2, 0) ,
VectorInt3 (-2, 1, 0) ,
VectorInt3 (0, 2, -1) ,
VectorInt3 (1, -2, 0) ,
VectorInt3 (1, 1, 2) ,
VectorInt3 (3, 0, 0) ,
VectorInt3 (1, 0, 2) ,
VectorInt3 (0, 1, -2) ,
VectorInt3 (2, 2, -1)
};

const VectorInt3 FeatureImplicitSolventd2::interactionShell_minusZ_on[24]={
VectorInt3 (1, 0, -3) ,
VectorInt3 (3, 0, -1) ,
VectorInt3 (-2, 1, -1) ,
VectorInt3 (1, -2, -1) ,
VectorInt3 (1, 1, -3) ,
VectorInt3 (2, 1, -2) ,
VectorInt3 (2, 2, -2) ,
VectorInt3 (-1, 1, -2) ,
VectorInt3 (2, -1, -2) ,
VectorInt3 (-1, 2, -2) ,
VectorInt3 (0, -2, -1) ,
VectorInt3 (0, 3, -1) ,
VectorInt3 (0, -1, -2) ,
VectorInt3 (1, -1, -2) ,
VectorInt3 (0, 0, -3) ,
VectorInt3 (3, 1, -1) ,
VectorInt3 (1, 2, -2) ,
VectorInt3 (-2, 0, -1) ,
VectorInt3 (2, 0, -2) ,
VectorInt3 (-1, 0, -2) ,
VectorInt3 (0, 2, -2) ,
VectorInt3 (1, 3, -1) ,
VectorInt3 (0, 1, -3) ,
VectorInt3 (-1, -1, -2)
};

const VectorInt3 FeatureImplicitSolventd2::interactionShell_minusZ_off[28]={
VectorInt3 (1, 0, 3) ,
VectorInt3 (1, 3, 1) ,
VectorInt3 (0, 1, 3) ,
VectorInt3 (1, 1, -1) ,
VectorInt3 (-1, -1, 2) ,
VectorInt3 (0, 2, 2) ,
VectorInt3 (0, 0, -1) ,
VectorInt3 (-2, 0, 1) ,
VectorInt3 (0, 0, 3) ,
VectorInt3 (3, 1, 1) ,
VectorInt3 (2, -1, 2) ,
VectorInt3 (-1, 0, 2) ,
VectorInt3 (1, 2, 2) ,
VectorInt3 (2, 0, 2) ,
VectorInt3 (0, -2, 1) ,
VectorInt3 (0, 3, 1) ,
VectorInt3 (-1, 1, 2) ,
VectorInt3 (2, 1, 2) ,
VectorInt3 (-1, 2, 2) ,
VectorInt3 (2, 2, 2) ,
VectorInt3 (0, -1, 2) ,
VectorInt3 (-2, 1, 1) ,
VectorInt3 (1, 1, 3) ,
VectorInt3 (0, 1, -1) ,
VectorInt3 (1, 0, -1) ,
VectorInt3 (1, -2, 1) ,
VectorInt3 (3, 0, 1) ,
VectorInt3 (1, -1, 2)
};

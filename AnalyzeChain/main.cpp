#include<cstring>
#include<vector>

#include <LeMonADE/core/Ingredients.h>
#include <LeMonADE/core/ConfigureSystem.h>
#include <LeMonADE/feature/FeatureMoleculesIO.h>
#include <LeMonADE/feature/FeatureAttributes.h>
#include <LeMonADE/utility/DepthIteratorPredicates.h>
#include <LeMonADE/utility/TaskManager.h>
#include <LeMonADE/utility/MonomerGroup.h>
#include <LeMonADE/utility/RandomNumberGenerators.h>
#include <LeMonADE/analyzer/AnalyzerWriteBfmFile.h>
#include <LeMonADE/analyzer/AnalyzerWriteBfmFileSubGroup.h>
#include <LeMonADE/updater/UpdaterSimpleSimulator.h>
#include <LeMonADE/updater/UpdaterReadBfmFile.h>

#include <LeMonADE-Extensions/features/FeatureExternalPotential.h>
#include <LeMonADE-Extensions/utilities/CommandlineParser.h>
#include <LeMonADE-Extensions/updaters/UpdaterBasicSimulator.h>
#include <LeMonADE-Extensions/updaters/UpdaterLinearChainCreator.h>
#include <LeMonADE-Extensions/analyzers/UnmovedMonomersAnalyzer.h>
#include <LeMonADE-Extensions/analyzers/LatticeOccupationAnalyzer.h>
#include <LeMonADE-Extensions/analyzers/AnalyzerFindPolymers.h>
#include <LeMonADE-Extensions/analyzers/AnalyzerFindMiddleMonomers.h>
#include <LeMonADE-Extensions/analyzers/AnalyzerMeanSquareDisplacement.h>
#include <LeMonADE-Extensions/analyzers/AnalyzerRadiusOfGyration.h>


#include <LeMonADE-ImplicitMembrane/AnalyzerProbabilityDensityZ.h>
#include <LeMonADE-ImplicitMembrane/AnalyzerAverageBondLength.h>
#include <LeMonADE-ImplicitMembrane/AnalyzerTranslocationCounter.h>
#include <LeMonADE-ImplicitMembrane/Potentials.h>


int main(int argc, char* argv[])
{

	std::string filename("config.bfm");
	bool msd=false;
	bool rg=false;
	bool pmf=false;
	bool trans=false;
	bool bonds=false;
	
	std::vector<int32_t> types(4,1);
	
	uint64_t lowerMcsLimit=10000000;
	double barrier1=0.0;
	double barrier2=0.0;
	
	////////////////////default values for command line parameters /////////////
	try{
		CommandLineParser cmd;
		cmd.addOption("--file",1,"input file");
		cmd.addOption("--msd",0,"analyze msd");
		cmd.addOption("--rg",0,"analyze rg");
		cmd.addOption("--pmf",0,"analyze pmf");
		cmd.addOption("--bonds",0,"analyze average bond length");
		cmd.addOption("--trans",2,"analyze translocations. arguments: start and end of barrier");
		cmd.addOption("--types",4,"monomer types considered in analysis. --types a b c d, double naming possible, default 1 1 1 1");
		cmd.addOption("--limit",1,"set lower mcs limit for analysis");
		cmd.addOption("--help",0,"display help");
		
		cmd.parse(argv+1,argc-1);
		
		if(cmd.getOption("--help")){cmd.displayHelp();exit(0);}
		
		cmd.getOption("--file",filename);
		
		msd=cmd.getOption("--msd");
		rg=cmd.getOption("--rg");
		pmf=cmd.getOption("--pmf");
		trans=cmd.getOption("--trans",barrier1,0);
		cmd.getOption("--trans",barrier2,1);
		
		bonds=cmd.getOption("--bonds");
		
		cmd.getOption("--types",types[0],0);
		cmd.getOption("--types",types[1],1);
		cmd.getOption("--types",types[2],2);
		cmd.getOption("--types",types[3],3);
		
		cmd.getOption("--limit",lowerMcsLimit);
	}
	catch(std::runtime_error& e){std::cerr<<"error while parsing command line: "<<e.what()<<std::endl;}
	catch(std::exception& e){std::cerr<<"error while parsing command line: "<<e.what()<<std::endl;}
	catch(...){std::cerr<<"unknown error while parsing command line"<<std::endl;}
	
	try{
		
		//////////// done getting command line parameters....start setting up the system ///
		///////////////////////////////////////////////////////////////////////
		
		//define ingredients
		typedef LOKI_TYPELIST_2(FeatureMoleculesIO,FeatureAttributes) Features;

		typedef ConfigureSystem<VectorInt3,Features> Config;
		typedef Ingredients<Config> Ing;
		Ing myIngredients;
		
		std::vector<MonomerGroup<typename Ing::molecules_type> > polymers;
		std::vector<MonomerGroup<typename Ing::molecules_type> > middleMonomers;
		
		TaskManager taskManager;
		
		taskManager.addUpdater(new UpdaterReadBfmFile<Ing>(filename,
								   myIngredients,
						     UpdaterReadBfmFile<Ing>::READ_STEPWISE));
		
		taskManager.addAnalyzer(new AnalyzerFindPolymers<Ing>(myIngredients,polymers,types));
		taskManager.addAnalyzer(new AnalyzerFindMiddleMonomers<Ing>(myIngredients,middleMonomers,types));
		
		if(msd){
			taskManager.addAnalyzer(new AnalyzerMeanSquareDisplacement<Ing>(myIngredients,middleMonomers,"middleMonomers"));
			taskManager.addAnalyzer(new AnalyzerMeanSquareDisplacement<Ing>(myIngredients,polymers,"COM"));
		}
		if(rg) taskManager.addAnalyzer(new AnalyzerRadiusOfGyrationOld<Ing>(myIngredients,polymers,"polymers",lowerMcsLimit));
		if(pmf) taskManager.addAnalyzer(new AnalyzerProbabilityDensity<Ing>(myIngredients,polymers));
		if(trans) taskManager.addAnalyzer(new AnalyzerTranslocationCounter<Ing>(myIngredients,polymers,barrier1,barrier2));
		if(bonds) taskManager.addAnalyzer(new AnalyzerAverageBondLength<Ing>(myIngredients,polymers,lowerMcsLimit));
		
		/*init and run the simulation*/
		taskManager.initialize();
		taskManager.run();
		taskManager.cleanup();
	}
	catch(std::runtime_error& e){std::cerr<<e.what()<<std::endl;}
	catch(std::exception& e){std::cerr<<e.what()<<std::endl;}
	catch(...){std::cerr<<"unknown error while running"<<std::endl;}
	return 0;
}



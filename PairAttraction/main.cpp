#include<cstring>
#include<vector>

#include <LeMonADE/core/Ingredients.h>
#include <LeMonADE/core/ConfigureSystem.h>
#include <LeMonADE/feature/FeatureMoleculesIO.h>
#include <LeMonADE/utility/DepthIterator.h>
#include <LeMonADE/utility/DepthIteratorPredicates.h>
#include <LeMonADE/utility/TaskManager.h>
#include <LeMonADE/utility/MonomerGroup.h>
#include <LeMonADE/utility/RandomNumberGenerators.h>
#include <LeMonADE/analyzer/AnalyzerWriteBfmFile.h>
#include <LeMonADE/analyzer/AnalyzerRadiusOfGyration.h>
#include <LeMonADE/updater/UpdaterSimpleSimulator.h>
#include <LeMonADE/updater/UpdaterReadBfmFile.h>


#include <LeMonADE-Extensions/features/FeatureExcludedVolumeSc.h>
#include <LeMonADE-Extensions/features/FeatureExternalPotential.h>
#include <LeMonADE-Extensions/utilities/CommandlineParser.h>
#include <LeMonADE-Extensions/updaters/UpdaterBasicSimulator.h>
#include <LeMonADE-Extensions/updaters/UpdaterLinearChainCreator.h>
#include <LeMonADE-Extensions/analyzers/UnmovedMonomersAnalyzer.h>
#include <LeMonADE-Extensions/analyzers/LatticeOccupationAnalyzer.h>
#include <LeMonADE-Extensions/analyzers/AnalyzerPerformance.h>

#include <LeMonADE-ImplicitMembrane/FeaturePairInteraction.h>
#include <LeMonADE-ImplicitMembrane/AnalyzerProbabilityDensityZ.h>
#include <LeMonADE-ImplicitMembrane/AnalyzerTranslocationCounter.h>
#include <LeMonADE-ImplicitMembrane/Potentials.h>
#include <LeMonADE-ImplicitMembrane/PairPotentials.h>

int main(int argc, char* argv[])
{

	////////////////////default values for command line parameters /////////////
	
	uint64_t upper_simulation_limit=50000000; //5e7mcs
	uint64_t save_interval=10000;
	uint32_t equilibration_steps=1000000; //1e6mcs
	
	int32_t box_x=64;
	int32_t box_y=64;
	int32_t box_z=64;
	
	//polymer definitions
	int32_t n_polymers=1;
	std::vector<int32_t> polymerComposition(32,1);
	
	//solvent settings
	double epsilonAttraction=-1.0; //magnitude of attractive implicit solvent interaction (should be negative)
	double rangeAttraction=4.0;
	
	std::vector<double> hydrophobicity(4,0.0); //hydrophobicities of monomer types
	
	//input/output settings
	std::string outputFilename;
	std::string inputFilename;
	
	double verletSkin=4.0;
	
	
	try{	
		//seed global random number generators
		RandomNumberGenerators randomNumbers;
		randomNumbers.seedAll();
		
		//////////////add command line arguments /////////////////////////////////
		CommandLineParser cmd;
		cmd.addOption("--max_mcs",1,"simulation runs to this mcs (default 1e8)");
		cmd.addOption("--save_interval",1,"mcs interval for saving configurations (default 50000)");
		cmd.addOption("--equilibration",1,"mcs for equilibration (default 1e7)");
		cmd.addOption("--box",3,"size of simulation box. --box x y z. default 64 64 64");
		cmd.addOption("--pair_pot",2,"set the strength and range of attraction for solvent interaction, syntax nrg, range");
		cmd.addOption("--use_verlet",1,"set options for verlet list. argument skin");
		cmd.addOption("--rcp",7,"add random copolymer(s) to system. syntax: nPolymers type1 n1 type2 n2 type3 n3 (default 1 polymers, type 1)");		
		cmd.addOption("--hydrophobicity",4,"adjust relative hydrophobicity of types 1-4. Default 0.0");
		cmd.addOption("--startconfig",1,"use file as start configuration. other conflicting options (e.g. box) are ignored");
		cmd.addOption("--help",0,"display help message");
		
		if ( argc < 2 || std::string(argv[1])=="--help" )
		  {std::cout<<"usage: program outputFilename [options]\n";cmd.displayHelp();exit(0);}
		
		//get output filename as first argument
		outputFilename=std::string(argv[1]);
		//////////////parse command line arguments /////////////////////////////////
		cmd.parse(argv+2,argc-2);
		cmd.getOption("--max_mcs",upper_simulation_limit);
		cmd.getOption("--save_interval",save_interval);
		cmd.getOption("--equilibration",equilibration_steps);
		cmd.getOption("--box",box_x,0);
		cmd.getOption("--box",box_y,1);
		cmd.getOption("--box",box_z,2);
		cmd.getOption("--pair_pot",epsilonAttraction,0);
		cmd.getOption("--pair_pot",rangeAttraction,1);
		cmd.getOption("--use_verlet",verletSkin);
		
		
		for(size_t n=0;n<4;n++)
			cmd.getOption("--hydrophobicity",hydrophobicity[n],n);
		if(cmd.getOption("--help"))
		  {std::cerr<<"usage: program filename [options]\n";cmd.displayHelp();exit(0);}
		
		if(cmd.getOption("--rcp",n_polymers,0))
		{
			polymerComposition.clear();
			uint32_t lenA,lenB,lenC;
			int32_t typeA,typeB,typeC;
			
			cmd.getOption("--rcp",typeA,1);
			cmd.getOption("--rcp",lenA,2);
			cmd.getOption("--rcp",typeB,3);
			cmd.getOption("--rcp",lenB,4);
			cmd.getOption("--rcp",typeC,5);
			cmd.getOption("--rcp",lenC,6);
			
			uint32_t na=0;uint32_t nb=0; uint32_t nc=0; uint32_t n=0;
			//used later for distributing the charges
			std::vector<int32_t> indicesA,indicesB,indicesC;
			
			while(n<lenA+lenB+lenC){
				uint32_t type=randomNumbers.r250_rand32()%(lenA+lenB+lenC);
				
				if(0<=type && type<lenA && na<lenA){
					polymerComposition.push_back(typeA);
					indicesA.push_back(n);
					na++;
					n++;
				
				}
				else if(lenA<=type && type<lenA+lenB && nb<lenB){
					polymerComposition.push_back(typeB);
					indicesB.push_back(n);
					nb++;
					n++;
				
				}
				else if(lenA+lenB<=type && type<lenA+lenB+lenC && nc<lenC){
					polymerComposition.push_back(typeC);
					indicesC.push_back(n);
					nc++;
					n++;
				
				}
				
			}
			
			std::cout<<"random copolymer sequence:";
			for(size_t n=0;n<polymerComposition.size();n++) std::cout<<polymerComposition[n];
			std::cout<<"\n";
			
			
		}
		
		
		
		//////////// done getting command line parameters....start setting up the system ///
		///////////////////////////////////////////////////////////////////////
		
		//define ingredients
		typedef LOKI_TYPELIST_3(FeatureMoleculesIO,
					FeatureExcludedVolumeSc<>,
					FeaturePairInteraction<PairPotentialSquare>) Features; //square pot needs to be checked!!!
		
// 		typedef LOKI_TYPELIST_2(FeatureMoleculesIO,
// 					FeaturePairInteraction<PairPotentialSquareEV>) Features; //square pot needs to be checked!!!
// 					
		typedef ConfigureSystem<VectorInt3,Features> Config;
		typedef Ingredients<Config> Ing;
		Ing myIngredients;
		
		
		TaskManager taskManager;
		
		//////// check validity of lattice occupation during setup and simulation ///
		//LatticeOccupationAnalyzer<Ing>* latticeCheck=new LatticeOccupationAnalyzer<Ing>(myIngredients);
		
		//this is used later when creating polymers and analyzing them
		std::vector<MonomerGroup<typename Ing::molecules_type> > polymersGroupVector;
		
		
		myIngredients.modifyPairPotential().setRange(rangeAttraction);
		
		for(int32_t a=1;a<=4;a++){
			for(int32_t b=a;b<=4;b++){
				
				double energy=epsilonAttraction*((hydrophobicity[a-1]+hydrophobicity[b-1])-std::fabs(hydrophobicity[a-1]-hydrophobicity[b-1]));
				myIngredients.modifyPairPotential().setInteraction(a,b,energy);
			
			}
			
		}
		
		
		if(cmd.getOption("--startconfig"))
		{
		  std::string inputFile;
		  cmd.getOption("--startconfig",inputFile);
		  std::cout<<"using file "<<inputFilename<<" as start configuration.\n";

		  UpdaterReadBfmFile<Ing>* fileReader=
		    new UpdaterReadBfmFile<Ing>(inputFilename,
						myIngredients,
						UpdaterReadBfmFile<Ing>::READ_LAST_CONFIG_FAST);

		  fileReader->initialize();
			
		  /*check if upper simulation limit is lower than the max mcs in the file*/
		  if(fileReader->getMaxAge()>=upper_simulation_limit)
		    std::cerr<<"requested upper mcs limit for simulation is lower than "
			     <<"the highest mcs in the file.\n";
			
		  taskManager.addUpdater(fileReader,0);
		  
		  
		}
		//if constructing new system
		else
		{
			/////////// set system parameters //////////////////////////////////////
			/*set up the box*/
			myIngredients.setBoxX(box_x);
			myIngredients.setBoxY(box_y);
			myIngredients.setBoxZ(box_z);
			myIngredients.setPeriodicX(1);
			myIngredients.setPeriodicY(1);
			myIngredients.setPeriodicZ(1);
			/*add the bondset*/
			myIngredients.modifyBondset().addBFMclassicBondset();
			
			/*synchronize to create lattice*/
			myIngredients.synchronize(myIngredients);
			//latticeCheck->initialize();
			
			/*now put the polymer in the system */
			UpdaterLinearChainCreator<Ing> polymerCreator(myIngredients,polymerComposition.size(),n_polymers);
			polymerCreator.resize(polymerComposition);
			polymerCreator.initialize();
			polymerCreator.execute();
			//latticeCheck->execute();
			
			myIngredients.synchronize(myIngredients);
			
		}

		myIngredients.setUseVerletList(cmd.getOption("--use_verlet"));
		myIngredients.setVerletSkin(verletSkin);
		myIngredients.synchronize();
		
		fill_connected_groups(myIngredients.getMolecules(),
				      polymersGroupVector,
				      MonomerGroup<typename Ing::molecules_type>(myIngredients.getMolecules()),
				      alwaysTrue());
			
		
		//calculate the intervals for short term analysis of translocation.
		uint64_t short_term_analysis_interval=100;
		int32_t execution_other_analyzers=save_interval/short_term_analysis_interval;
		int32_t execution_lattice_check=100*execution_other_analyzers;
		
		
		//this is run only once at the beginning for equilibration.
		taskManager.addUpdater(new UpdaterSimpleSimulator<Ing,MoveLocalSc>(myIngredients,
										   equilibration_steps),
				       0);

		taskManager.addUpdater(new UpdaterBasicSimulator<Ing,MoveLocalSc>(myIngredients,
										  short_term_analysis_interval,
										  upper_simulation_limit),
				       1);
		
		/*add system consistency checker*/
		//taskManager.addAnalyzer(latticeCheck,execution_lattice_check);
		taskManager.addAnalyzer(new UnmovedMonomersAnalyzer<Ing>(myIngredients),execution_lattice_check);
		taskManager.addAnalyzer(new AnalyzerPerformance<Ing>(myIngredients),execution_other_analyzers);
		taskManager.addAnalyzer(new AnalyzerRadiusOfGyration<Ing>(myIngredients,
									  "polymer"),
					execution_other_analyzers);
		taskManager.addAnalyzer(new AnalyzerProbabilityDensity<Ing>(myIngredients,polymersGroupVector),execution_other_analyzers);
		
// 		double border1=double(box_z)/2.0-membraneWidth-8.0;
// 		double border2=double(box_z)/2.0+membraneWidth+8.0;
// 		taskManager.addAnalyzer(new AnalyzerTranslocationCounter<Ing>(myIngredients,polymersGroupVector,border1,border2),1);
		
		taskManager.addAnalyzer(new AnalyzerWriteBfmFile<Ing>(outputFilename,
								      myIngredients,
								      AnalyzerWriteBfmFile<Ing>::NEWFILE),
					execution_other_analyzers);
		
		/*init and run the simulation*/
		taskManager.initialize();
		taskManager.run();
		taskManager.cleanup();
	}
	catch(std::runtime_error& e){std::cerr<<e.what()<<std::endl;}
	catch(std::exception& e){std::cerr<<e.what()<<std::endl;}
	
	return 0;
}




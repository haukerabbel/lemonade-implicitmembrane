#ifndef PAIR_POTENTIALS_H
#define PAIR_POTENTIALS_H

#include<cmath>

#include <LeMonADE/utility/DistanceCalculation.h>

class PairPotentialSquare{
public:
	PairPotentialSquare():rangeSquared(0)
	{
		energy.resize(10,std::vector<double>(10,0.0));
		probability.resize(10,std::vector<double>(10,1.0));
		
	}
	
	void setInteraction(int32_t typeA,int32_t typeB,double nrg)
	{
		energy.at(typeA-1).at(typeB-1)=nrg;
		energy.at(typeB-1).at(typeA-1)=nrg;
	}
	
	double getInteraction(int32_t typeA,int32_t typeB) const {return energy.at(typeA-1).at(typeB-1);}
	
	void setRange(double r){rangeSquared=int32_t(r*r);}
	double getRange() const {return std::sqrt(rangeSquared);}
	
	template<class IngredientsType> double getEnergy(VectorInt3 posA,VectorInt3 posB, int32_t typeA, int32_t typeB, const IngredientsType& ing) const
	{
		VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ing);
		
		if(distVector*distVector<=rangeSquared){
			return energy.at(typeA-1).at(typeB-1);
		}
		
		else return 0.0;
	}
	
	template<class IngredientsType> double getExpEnergy(VectorInt3 posA,VectorInt3 posB, int32_t typeA, int32_t typeB, const IngredientsType& ing) const
	{
		VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ing);
		
		if(distVector*distVector<=rangeSquared){
			return probability.at(typeA-1).at(typeB-1);
		}
		
		else return 1.0;
		
	}
	
	template<class IngredientsType> void synchronize(const IngredientsType& ing)
	{
		std::cout<<"PairPotentialSquareNNI range set to sqrt("<<rangeSquared<<")\n";
		for(size_t n=0;n<energy.size();n++){
			for(size_t m=0;m<energy.size();m++){
				if(energy[n][m]!=0.0) probability[n][m]=std::exp(-energy[n][m]);
			}
		}
	}
	
private:
	std::vector<std::vector<double> > energy;
	std::vector<std::vector<double> > probability;
	
	int32_t rangeSquared;
	
	
};

class PairPotentialSquareEV{
public:
	PairPotentialSquareEV():rangeSquared(0)
	{
		energy.resize(10,std::vector<double>(10,0.0));
		probability.resize(10,std::vector<double>(10,1.0));
		
	}
	
	void setInteraction(int32_t typeA,int32_t typeB,double nrg)
	{
		energy.at(typeA-1).at(typeB-1)=nrg;
		energy.at(typeB-1).at(typeA-1)=nrg;
	}
	
	double getInteraction(int32_t typeA,int32_t typeB) const {return energy.at(typeA-1).at(typeB-1);}
	
	void setRange(double r){rangeSquared=int32_t(r*r);}
	double getRange() const {return std::sqrt(rangeSquared);}
	
	template<class IngredientsType> double getEnergy(VectorInt3 posA,VectorInt3 posB, int32_t typeA, int32_t typeB, const IngredientsType& ing) const
	{
		VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ing);
		int32_t dist2=distVector*distVector;
		
		if(dist2<4){
			return std::numeric_limits< double >::infinity();
		}
		else if(dist2<=rangeSquared){
			return energy.at(typeA-1).at(typeB-1);
		}
		
		else return 0.0;
	}
	
	template<class IngredientsType> double getExpEnergy(VectorInt3 posA,VectorInt3 posB, int32_t typeA, int32_t typeB, const IngredientsType& ing) const
	{
		VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ing);
		int32_t dist2=distVector*distVector;
		
		if(dist2<4){
			return 0.0;
		}
		else if(dist2<=rangeSquared){
			return probability.at(typeA-1).at(typeB-1);
		}
		
		else return 1.0;
		
	}
	
	template<class IngredientsType> void synchronize(const IngredientsType& ing)
	{
		std::cout<<"PairPotentialSquareNNI range set to sqrt("<<rangeSquared<<")\n";
		for(size_t n=0;n<energy.size();n++){
			for(size_t m=0;m<energy.size();m++){
				if(energy[n][m]!=0.0) probability[n][m]=std::exp(-energy[n][m]);
			}
		}
	}
	
private:
	std::vector<std::vector<double> > energy;
	std::vector<std::vector<double> > probability;
	
	int32_t rangeSquared;
	
	
};

class PairPotentialSquareNNI{
public:
	PairPotentialSquareNNI()
	:rangeSquared(0),nnEnergy(0.0)
	,expNNEnergy(1.0),exp2NNEnergy(1.0),exp4NNEnergy(1.0)
	{
		energy.resize(10,std::vector<double>(10,0.0));
		probability.resize(10,std::vector<double>(10,1.0));
		
	}
	
	void setInteraction(int32_t typeA,int32_t typeB,double nrg)
	{
		energy.at(typeA-1).at(typeB-1)=nrg;
		energy.at(typeB-1).at(typeA-1)=nrg;
	}
	
	double getInteraction(int32_t typeA,int32_t typeB) const {return energy.at(typeA-1).at(typeB-1);}
	
	void setRange(double r){rangeSquared=int32_t(r*r);}
	double getRange() const {return std::sqrt(rangeSquared);}
	
	void setNNRepulsion(double nrg){nnEnergy=nrg;}
	double getNNRepultion()const{return nnEnergy;}
	
	template<class IngredientsType> double getEnergy(VectorInt3 posA,VectorInt3 posB, int32_t typeA, int32_t typeB, const IngredientsType& ing) const
	{
		VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ing);
		int32_t dist2=distVector*distVector;
		
		double returnNRG=0.0;
		
		if(dist2<4){
			returnNRG= std::numeric_limits< double >::infinity();
		}
		else if(dist2<=rangeSquared){
			returnNRG+= energy.at(typeA-1).at(typeB-1);
			
			if(dist2==4){
				returnNRG+= 4*nnEnergy;
			}
			else if(dist2==5){
				returnNRG+= 2*nnEnergy;
			}
			else if(dist2==6){
				returnNRG+= nnEnergy;
			}
		}
		
		
		return returnNRG;
	}
	
	template<class IngredientsType> double getExpEnergy(VectorInt3 posA,VectorInt3 posB, int32_t typeA, int32_t typeB, const IngredientsType& ing) const
	{
		VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ing);
		int32_t dist2=distVector*distVector;
		
		double returnProb=1.0;
		
		if(dist2<4){
			returnProb=0.0;
		}
		else if(dist2<=rangeSquared){
			returnProb*=probability.at(typeA-1).at(typeB-1);
			
			if(dist2==4){
				returnProb*= exp4NNEnergy;
			}
			else if(dist2==5){
				returnProb*= exp2NNEnergy;
			}
			else if(dist2==6){
				returnProb*= expNNEnergy;
			}
		}
		
		return returnProb;
		
	}
	
	template<class IngredientsType> void synchronize(const IngredientsType& ing)
	{
		std::cout<<"PairPotentialSquareNNI range set to sqrt("<<rangeSquared<<")\n";
		for(size_t n=0;n<energy.size();n++){
			for(size_t m=0;m<energy.size();m++){
				if(energy[n][m]!=0.0) probability[n][m]=std::exp(-energy[n][m]);
			}
		}
		expNNEnergy=std::exp(-nnEnergy);
		exp2NNEnergy=std::exp(-2.0*nnEnergy);
		exp4NNEnergy=std::exp(-4.0*nnEnergy);
	}
	
private:
	std::vector<std::vector<double> > energy;
	std::vector<std::vector<double> > probability;
	
	int32_t rangeSquared;
	double nnEnergy;
	double expNNEnergy;
	double exp2NNEnergy;
	double exp4NNEnergy;
};

class PairPotentialSquareNNI_Hdep{
public:
	PairPotentialSquareNNI_Hdep()
	:rangeSquared(0)
	{
		for(size_t a=0;a<10;a++){
			for(size_t b=0;b<10;b++){
				attractionConst[a][b]=0.0;
				nnInteractionConst[a][b]==0.0;
				
				for(int32_t r2=0;r2<64;r2++){
					
					if(r2<4){
						nrg[a][b][r2]=std::numeric_limits< double >::infinity();
						probability[a][b][r2]=0.0;
					}
					else{
						nrg[a][b][r2]=0.0;
						probability[a][b][r2]=1.0;
					}
					
				}
			}
		}
		
	}
	
	void setAttraction(int32_t typeA,int32_t typeB,double nrg)
	{
		attractionConst[typeA-1][typeB-1]=nrg;
		attractionConst[typeB-1][typeA-1]=nrg;
	}
	
	double getAttraction(int32_t typeA,int32_t typeB) const {return attractionConst[typeA-1][typeB-1];}
	
	void setRange(double r){rangeSquared=int32_t(r*r);}
	double getRange() const {return std::sqrt(rangeSquared);}
	
	void setNNRepulsion(int32_t typeA,int32_t typeB,double nrg){
		nnInteractionConst[typeA-1][typeB-1]=nrg;
		nnInteractionConst[typeB-1][typeA-1]=nrg;
	}
	double getNNRepultion(int32_t typeA,int32_t typeB)const{return nnInteractionConst[typeA-1][typeB-1];}
	
	template<class IngredientsType> double getEnergy(VectorInt3 posA,VectorInt3 posB, int32_t typeA, int32_t typeB, const IngredientsType& ing) const
	{
		VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ing);
		int32_t dist2=distVector*distVector;
		
		if(typeA>0 && typeB>0 && typeA<11 && typeB<11)
		{
			if(dist2<64)
				return nrg[typeA-1][typeB-1][dist2];
			else return 0.0;
		}
		else{
			throw std::runtime_error("pair potential type too large\n");
		}
	}
	
	template<class IngredientsType> double getExpEnergy(VectorInt3 posA,VectorInt3 posB, int32_t typeA, int32_t typeB, const IngredientsType& ing) const
	{
		VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ing);
		int32_t dist2=distVector*distVector;
		if(dist2<64)
			return probability[typeA-1][typeB-1][dist2];
		else return 1.0;
		
	}
	
	template<class IngredientsType> void synchronize(const IngredientsType& ing)
	{
		for(size_t a=0;a<10;a++){
			for(size_t b=0;b<10;b++){
				for(int32_t r2=0;r2<64;r2++){
					
					if(r2<4){
						nrg[a][b][r2]=std::numeric_limits< double >::infinity();
						//probability[a][b][r2]=0.0;
					}
					else if(r2<=rangeSquared){
						nrg[a][b][r2]=attractionConst[a][b];
						
					}
					else{
						nrg[a][b][r2]=0.0;
						
					}
					if(r2==4) nrg[a][b][r2]+=4.0*nnInteractionConst[a][b]; 
					else if(r2==5) nrg[a][b][r2]+=2.0*nnInteractionConst[a][b];
					else if(r2==6) nrg[a][b][r2]+=1.0*nnInteractionConst[a][b];
					
					probability[a][b][r2]=std::exp(-nrg[a][b][r2]);
					
				}
			}
		}

	}
	
private:
	int32_t rangeSquared;
	double attractionConst[10][10];
	double nnInteractionConst[10][10];
	double nrg[10][10][64];
	double probability[10][10][64];
};

class PairPotentialSquareNNI_long_Hdep{
public:
	PairPotentialSquareNNI_long_Hdep()
	:rangeSquared(0)
	{
		for(size_t a=0;a<10;a++){
			for(size_t b=0;b<10;b++){
				attractionConst[a][b]=0.0;
				nnInteractionConst[a][b]==0.0;
				
				for(int32_t r2=0;r2<64;r2++){
					
					if(r2<4){
						nrg[a][b][r2]=std::numeric_limits< double >::infinity();
						probability[a][b][r2]=0.0;
					}
					else{
						nrg[a][b][r2]=0.0;
						probability[a][b][r2]=1.0;
					}
					
				}
			}
		}
		
	}
	
	void setAttraction(int32_t typeA,int32_t typeB,double nrg)
	{
		attractionConst[typeA-1][typeB-1]=nrg;
		attractionConst[typeB-1][typeA-1]=nrg;
	}
	
	double getAttraction(int32_t typeA,int32_t typeB) const {return attractionConst[typeA-1][typeB-1];}
	
	void setRange(double r){rangeSquared=int32_t(r*r);}
	double getRange() const {return std::sqrt(rangeSquared);}
	
	void setNNRepulsion(int32_t typeA,int32_t typeB,double nrg){
		nnInteractionConst[typeA-1][typeB-1]=nrg;
		nnInteractionConst[typeB-1][typeA-1]=nrg;
	}
	double getNNRepultion(int32_t typeA,int32_t typeB)const{return nnInteractionConst[typeA-1][typeB-1];}
	
	template<class IngredientsType> double getEnergy(VectorInt3 posA,VectorInt3 posB, int32_t typeA, int32_t typeB, const IngredientsType& ing) const
	{
		VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ing);
		int32_t dist2=distVector*distVector;
		
		if(typeA>0 && typeB>0 && typeA<11 && typeB<11)
		{
			if(dist2<64)
				return nrg[typeA-1][typeB-1][dist2];
			else return 0.0;
		}
		else{
			throw std::runtime_error("pair potential type too large\n");
		}
	}
	
	template<class IngredientsType> double getExpEnergy(VectorInt3 posA,VectorInt3 posB, int32_t typeA, int32_t typeB, const IngredientsType& ing) const
	{
		VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ing);
		int32_t dist2=distVector*distVector;
		if(dist2<64)
			return probability[typeA-1][typeB-1][dist2];
		else return 1.0;
		
	}
	
	template<class IngredientsType> void synchronize(const IngredientsType& ing)
	{
		for(size_t a=0;a<10;a++){
			for(size_t b=0;b<10;b++){
				for(int32_t r2=0;r2<64;r2++){
					
					if(r2<4){
						nrg[a][b][r2]=std::numeric_limits< double >::infinity();
						//probability[a][b][r2]=0.0;
					}
					else if(r2<=rangeSquared){
						nrg[a][b][r2]=attractionConst[a][b];
						
					}
					else{
						nrg[a][b][r2]=0.0;
						
					}
					if(r2==4) nrg[a][b][r2]+=4.0*nnInteractionConst[a][b]; 
					else if(r2==5) nrg[a][b][r2]+=2.0*nnInteractionConst[a][b];
					else if(r2==6 || r2==8) nrg[a][b][r2]+=1.0*nnInteractionConst[a][b];
					
					probability[a][b][r2]=std::exp(-nrg[a][b][r2]);
					
				}
			}
		}

	}
	
private:
	int32_t rangeSquared;
	double attractionConst[10][10];
	double nnInteractionConst[10][10];
	double nrg[10][10][64];
	double probability[10][10][64];
};


class PairPotentialSquare_membrane{
public:
	PairPotentialSquare_membrane()
	:rangeSquared(0)
	,membraneWidth(0)
	,membraneHigh(0)
	,membraneLow(0)
	,interactionConstant(0.0)
	,nnRepulsionConstant(0.0)
	{
		for(size_t a=0;a<10;a++){
			for(size_t b=0;b<10;b++){
				for(int32_t r2=0;r2<64;r2++){
					
					if(r2<4){
						nrg_mA_mB[a][b][r2]=std::numeric_limits< double >::infinity();
						nrg_mA_wB[a][b][r2]=std::numeric_limits< double >::infinity();
						nrg_wA_mB[a][b][r2]=std::numeric_limits< double >::infinity();
						nrg_wA_wB[a][b][r2]=std::numeric_limits< double >::infinity();
						prob_mA_mB[a][b][r2]=0.0;
						prob_mA_wB[a][b][r2]=0.0;
						prob_wA_mB[a][b][r2]=0.0;
						prob_wA_wB[a][b][r2]=0.0;
					}
					else{
						nrg_mA_mB[a][b][r2]=0.0;
						nrg_mA_wB[a][b][r2]=0.0;
						nrg_wA_mB[a][b][r2]=0.0;
						nrg_wA_wB[a][b][r2]=0.0;
						prob_mA_mB[a][b][r2]=1.0;
						prob_mA_wB[a][b][r2]=1.0;
						prob_wA_mB[a][b][r2]=1.0;
						prob_wA_wB[a][b][r2]=1.0;;
					}
					
				}
			}
		}
		
	}
	
	void setHydrophobicity(int32_t type, double H){hydrophobicity[type]=H;}
	double getHydrophobicity(int32_t type) {return hydrophobicity[type];}
	
	void setRange(double r){rangeSquared=int32_t(r*r);}
	double getRange() const {return std::sqrt(rangeSquared);}
	
	void setNNRepulsion(double nrg){nnRepulsionConstant=nrg;}
	double getNNRepultion()const{return nnRepulsionConstant;}
	
	void setMembraneWidth(int32_t w){membraneWidth=w;}
	int32_t getMembraneWidth()const{return membraneWidth;}
	
	void setInteractionConstant(double e){interactionConstant=e;}
	double getInteractionConstant()const{return interactionConstant;}
	
	bool inMembrane(VectorInt3 pos) const{
		int32_t z=pos.getZ();
		while (z<0) z+=(boxZm1+1);
		z&=boxZm1;
		if(z<membraneHigh && z>=membraneLow ) return true;
		else return false;
	}
	
	template<class IngredientsType> double getEnergy(VectorInt3 posA,VectorInt3 posB, int32_t typeA, int32_t typeB, const IngredientsType& ing) const
	{
		VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ing);
		int32_t dist2=distVector*distVector;
		
		if(dist2>=64) return 0.0;
		
		else if(inMembrane(posA) && inMembrane(posB)){
			return nrg_mA_mB[typeA-1][typeB-1][dist2];
		}
		else if(inMembrane(posA) && !inMembrane(posB)){
			return nrg_mA_wB[typeA-1][typeB-1][dist2];
		}
		else if(!inMembrane(posA) && inMembrane(posB)){
			return nrg_wA_mB[typeA-1][typeB-1][dist2];
		}
		else{/* both outside membrane*/
			return nrg_wA_wB[typeA-1][typeB-1][dist2];
		}
		
	}
	
	template<class IngredientsType> double getExpEnergy(VectorInt3 posA,VectorInt3 posB, int32_t typeA, int32_t typeB, const IngredientsType& ing) const
	{
		VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ing);
		int32_t dist2=distVector*distVector;
		
		if(dist2>=64) return 1.0;
		
		else if(inMembrane(posA) && inMembrane(posB)){
			return prob_mA_mB[typeA-1][typeB-1][dist2];
		}
		else if(inMembrane(posA) && !inMembrane(posB)){
			return prob_mA_wB[typeA-1][typeB-1][dist2];
		}
		else if(!inMembrane(posA) && inMembrane(posB)){
			return prob_wA_mB[typeA-1][typeB-1][dist2];
		}
		else{/* both outside membrane*/
			return prob_wA_wB[typeA-1][typeB-1][dist2];
		}
		
	}
	
	template<class IngredientsType> void synchronize(const IngredientsType& ing)
	{
		for(size_t a=0;a<10;a++){
			for(size_t b=0;b<10;b++){
				
				for(int32_t r2=0;r2<64;r2++){
					
					if(r2<4){
						nrg_mA_mB[a][b][r2]=std::numeric_limits< double >::infinity();
						nrg_mA_wB[a][b][r2]=std::numeric_limits< double >::infinity();
						nrg_wA_mB[a][b][r2]=std::numeric_limits< double >::infinity();
						nrg_wA_wB[a][b][r2]=std::numeric_limits< double >::infinity();
					}
					else if(r2<=rangeSquared){
						//using a+1 in the hydrophobicity here, because they are saved as a map of the real type value (not -1) and the loop 
						//ranges from 0 to 9, so we need range 1-10 in hydrophobicity
						nrg_wA_wB[a][b][r2]=interactionConstant*((hydrophobicity[a+1]+hydrophobicity[b+1])-std::fabs(hydrophobicity[a+1]-hydrophobicity[b+1]));
						nrg_wA_mB[a][b][r2]=interactionConstant*(1.0+(hydrophobicity[a+1]-hydrophobicity[b+1])-std::fabs(hydrophobicity[a+1]-hydrophobicity[b+1]));
						nrg_mA_wB[a][b][r2]=interactionConstant*(1.0+(-hydrophobicity[a+1]+hydrophobicity[b+1])-std::fabs(hydrophobicity[a+1]-hydrophobicity[b+1]));
						nrg_mA_mB[a][b][r2]=interactionConstant*(2.0-(hydrophobicity[a+1]+hydrophobicity[b+1])-std::fabs(hydrophobicity[a+1]-hydrophobicity[b+1]));
						
					}
					else{
						nrg_mA_mB[a][b][r2]=0.0;
						nrg_mA_wB[a][b][r2]=0.0;
						nrg_wA_mB[a][b][r2]=0.0;
						nrg_wA_wB[a][b][r2]=0.0;
					}
					
					if(r2==4){ 
						nrg_mA_mB[a][b][r2]+=4.0*nrg_mA_mB[a][b][r2]/interactionConstant*nnRepulsionConstant; 
						nrg_mA_wB[a][b][r2]+=4.0*nrg_mA_wB[a][b][r2]/interactionConstant*nnRepulsionConstant;
						nrg_wA_mB[a][b][r2]+=4.0*nrg_wA_mB[a][b][r2]/interactionConstant*nnRepulsionConstant;
						nrg_wA_wB[a][b][r2]+=4.0*nrg_wA_wB[a][b][r2]/interactionConstant*nnRepulsionConstant;
					}
					else if(r2==5){
						nrg_mA_mB[a][b][r2]+=2.0*nrg_mA_mB[a][b][r2]/interactionConstant*nnRepulsionConstant;
						nrg_mA_wB[a][b][r2]+=2.0*nrg_mA_wB[a][b][r2]/interactionConstant*nnRepulsionConstant;
						nrg_wA_mB[a][b][r2]+=2.0*nrg_wA_mB[a][b][r2]/interactionConstant*nnRepulsionConstant;
						nrg_wA_wB[a][b][r2]+=2.0*nrg_wA_wB[a][b][r2]/interactionConstant*nnRepulsionConstant;
					}
					else if(r2==6){
						nrg_mA_mB[a][b][r2]+=1.0*nrg_mA_mB[a][b][r2]/interactionConstant*nnRepulsionConstant;
						nrg_mA_wB[a][b][r2]+=1.0*nrg_mA_wB[a][b][r2]/interactionConstant*nnRepulsionConstant;
						nrg_wA_mB[a][b][r2]+=1.0*nrg_wA_mB[a][b][r2]/interactionConstant*nnRepulsionConstant;
						nrg_wA_wB[a][b][r2]+=1.0*nrg_wA_wB[a][b][r2]/interactionConstant*nnRepulsionConstant;
					}
					
					prob_mA_mB[a][b][r2]=std::exp(-nrg_mA_mB[a][b][r2]);
					prob_mA_wB[a][b][r2]=std::exp(-nrg_mA_wB[a][b][r2]);
					prob_wA_mB[a][b][r2]=std::exp(-nrg_wA_mB[a][b][r2]);
					prob_wA_wB[a][b][r2]=std::exp(-nrg_wA_wB[a][b][r2]);
				}
			}
		}
		
		
		boxZm1=ing.getBoxZ()-1;
		membraneCenter=ing.getBoxZ()/2;
		membraneLow=membraneCenter-membraneWidth;
		membraneHigh=membraneCenter+membraneWidth;
		
		std::cout<<"\nPairPotentialSquare_membrane: final interaction table M-M up to r=6:\n";
		std::cout<<"a\tb\t dist=";
		for(int32_t r=0;r<=6;r++){
			std::cout<<r<<"\t";
		}
		std::cout<<std::endl;
		for(size_t a=0;a<10;a++){
			for(size_t b=0;b<10;b++){
				std::cout<<a<<"\t"<<b<<"\t";
				for(int32_t r=0;r<=6;r++){
					std::cout<<nrg_mA_mB[a][b][r*r]<<"\t";
				}
				std::cout<<std::endl;
			}
		}
		std::cout<<"\nPairPotentialSquare_membrane: final interaction table W-M up to r=6:\n";
		std::cout<<"a\tb\t dist=";
		for(int32_t r=0;r<=6;r++){
			std::cout<<r<<"\t";
		}
		std::cout<<std::endl;
		for(size_t a=0;a<10;a++){
			for(size_t b=0;b<10;b++){
				std::cout<<a<<"\t"<<b<<"\t";
				for(int32_t r=0;r<=6;r++){
					std::cout<<nrg_wA_mB[a][b][r*r]<<"\t";
				}
				std::cout<<std::endl;
			}
		}
		
		
		std::cout<<"\nPairPotentialSquare_membrane: final interaction table M-W up to r=6:\n";
		std::cout<<"a\tb\t dist=";
		for(int32_t r=0;r<=6;r++){
			std::cout<<r<<"\t";
		}
		std::cout<<std::endl;
		for(size_t a=0;a<10;a++){
			for(size_t b=0;b<10;b++){
				std::cout<<a<<"\t"<<b<<"\t";
				for(int32_t r=0;r<=6;r++){
					std::cout<<nrg_mA_wB[a][b][r*r]<<"\t";
				}
				std::cout<<std::endl;
			}
		}
		
		std::cout<<"\nPairPotentialSquare_membrane: final interaction table W-W up to r=6:\n";
		std::cout<<"a\tb\t dist=";
		for(int32_t r=0;r<=6;r++){
			std::cout<<r<<"\t";
		}
		std::cout<<std::endl;
		for(size_t a=0;a<10;a++){
			for(size_t b=0;b<10;b++){
				std::cout<<a<<"\t"<<b<<"\t";
				for(int32_t r=0;r<=6;r++){
					std::cout<<nrg_wA_wB[a][b][r*r]<<"\t";
				}
				std::cout<<std::endl;
			}
		}

	}
	
private:
	std::map<int32_t,double> hydrophobicity;
	int32_t membraneWidth;
	int32_t membraneCenter,membraneLow,membraneHigh;
	int32_t rangeSquared;
	int32_t boxZm1;
	double interactionConstant; 
	double nnRepulsionConstant;
	
	double nnInteractionConst[10][10];
	double nrg_mA_mB[10][10][64];
	double nrg_mA_wB[10][10][64];
	double nrg_wA_mB[10][10][64];
	double nrg_wA_wB[10][10][64];
	
	double prob_mA_mB[10][10][64];
	double prob_mA_wB[10][10][64];
	double prob_wA_mB[10][10][64];
	double prob_wA_wB[10][10][64];
	
};
#endif /*PAIR_POTENTIALS_H*/
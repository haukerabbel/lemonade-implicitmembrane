#ifndef ANALYZER_PROBABILITY_DENSITY_H
#define ANALYZER_PROBABILITY_DENSITY_H

#include <LeMonADE/analyzer/AbstractAnalyzer.h>
#include <LeMonADE/utility/ResultFormattingTools.h>
#include <LeMonADE/utility/Vector3D.h>

#include <LeMonADE-Extensions/utilities/Histogram1D.h>

/************************************************************************
 * calculates the probability histogram of the coordinates of a group of
 * particles in a box
 * **********************************************************************/


template < class IngredientsType > class AnalyzerProbabilityDensity : public AbstractAnalyzer
{
	typedef typename IngredientsType::molecules_type molecules_type;
	
	const IngredientsType& ingredients;
	
	const std::vector<MonomerGroup<molecules_type> >& groups;
	
	//TimeSeries<VectorDouble3>  posZTimeSeries;
	Histogram1D probX,probY,probZ;
	
	template < class MoleculesType > VectorDouble3 centerOfMass(const MoleculesType& m) const
	{
		VectorDouble3 CoM_sum;    
		for ( uint i = 0; i < m.size(); ++i)
		{
			CoM_sum.setX( CoM_sum.getX() + m[i].getX() );
			CoM_sum.setY( CoM_sum.getY() + m[i].getY() );
			CoM_sum.setZ( CoM_sum.getZ() + m[i].getZ() );
		}
		
		double inv_N = 1.0 / double ( m.size() );
		
		VectorDouble3 CoM (
			double ( CoM_sum.getX() ) * inv_N, 
				   double ( CoM_sum.getY() ) * inv_N,
				   double ( CoM_sum.getZ() ) * inv_N);
		
		return CoM;
		
	}
	
public:
  
	AnalyzerProbabilityDensity(const IngredientsType& ing, const std::vector<MonomerGroup<molecules_type> >& g);
	
	virtual ~AnalyzerProbabilityDensity(){}
	
	virtual void initialize();
	virtual bool execute();
	virtual void cleanup();
	
	double foldBack(double value, int32_t box) const {
			
		//make coordinate positive, so that modulo is properly defined
		while (value < 0.0) {
			value += double(box);
		}
		while (value >= double(box)) {
			value -= double(box);
		}
		return value;
	}

	
};

/*************************************************************************
 * implementation of memeber execute()
 * ***********************************************************************/

template<class IngredientsType>
AnalyzerProbabilityDensity<IngredientsType>::AnalyzerProbabilityDensity(const IngredientsType& ing, const std::vector<MonomerGroup< molecules_type > >& g)
:ingredients(ing),groups(g)
{
	
}


template< class IngredientsType>
void AnalyzerProbabilityDensity<IngredientsType>::initialize()
{	
	probX.reset(0.0,double(ingredients.getBoxX()),ingredients.getBoxX());
	probY.reset(0.0,double(ingredients.getBoxY()),ingredients.getBoxY());
	probZ.reset(0.0,double(ingredients.getBoxZ()),ingredients.getBoxZ());
}

template< class IngredientsType >
bool AnalyzerProbabilityDensity<IngredientsType>::execute()
{
	
	for(size_t n=0;n<groups.size();n++)
	{
		VectorDouble3 pos=centerOfMass(groups[n]);
		
		
		probX.addValue(foldBack(pos.getX(),ingredients.getBoxX()));
		probY.addValue(foldBack(pos.getY(),ingredients.getBoxY()));
		probZ.addValue(foldBack(pos.getZ(),ingredients.getBoxZ()));
		
	}
	
	return true;
}

template<class IngredientsType>
void AnalyzerProbabilityDensity<IngredientsType>::cleanup()
{
	
	//now write to file (including a comment)
	std::stringstream comment;
	comment<<"ProbabilityDensity\n";
	comment<<"sample size: "<<groups.size()<<" groups\n";
	
	comment<<"format: z prob_x prob_y prob_z\n";
	
	std::vector<std::vector<double> > probDens;
	probDens.resize(4);
	
	probDens[0]=probX.getVectorBins();
	probDens[1]=probX.getVectorValuesNormalized();
	probDens[2]=probY.getVectorValuesNormalized();
	probDens[3]=probZ.getVectorValuesNormalized();
	
	ResultFormattingTools::writeResultFile("ProbabilityDensity.dat",ingredients,probDens,comment.str());
	
	
	
}




#endif



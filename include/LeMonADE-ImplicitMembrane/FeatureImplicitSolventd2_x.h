#ifndef FEATURE_IMPLICIT_SOLVENT_D2_X_H
#define FEATURE_IMPLICIT_SOLVENT_D2_X_H

/**
 * @file
 * @brief class FeatureImplicitSolvent
 * */

#include <LeMonADE/feature/Feature.h>
#include <LeMonADE/feature/FeatureLatticePowerOfTwo.h>
#include <LeMonADE/feature/FeatureBoltzmann.h>
#include <LeMonADE/feature/FeatureAttributes.h>
#include <LeMonADE/updater/moves/MoveBase.h>
#include <LeMonADE/updater/moves/MoveLocalSc.h>
#include <LeMonADE/updater/moves/MoveAddMonomerSc.h>

#include <LeMonADE-ImplicitMembrane/SolventDistribution.h>

/******************************************************************************/
/**
 * @class FeatureImplicitSolventd2_X
 * @brief Feature adding excluded volume and position dependent implicit solvent
 * @details The feature takes care of excluded volume and allows to add two types
 * of interaction:
 * 1) contact interaction of monomers
 * 2) interaction of monomer with surrounding empty lattice sites.
 * Number 1) adds a energy penalty on contact with lattice site occpied by monomer
 * This can be attributed to an attribute tag, e.g. type1-type2: 0.8kT
 * Number 2 adds type dependent energy penalty on contact with empty lattice site.
 * 
 * Works with MoveLocalSc and MoveAddMonomerSc.
 * */
/******************************************************************************/

class FeatureImplicitSolventd2_X:public Feature{
public:
  //this feature requires a lattice.
  typedef LOKI_TYPELIST_2(FeatureLatticePowerOfTwo<uint8_t>,FeatureAttributes) required_features_front;
  typedef LOKI_TYPELIST_1(FeatureBoltzmann) required_features_back;
  //constructor
  FeatureImplicitSolventd2_X();
	
  void setSolventDistribution(SolventDistribution* pSolventDist);

  void setSolventInteraction(uint32_t solventType, uint32_t monoType,double energy);
  double getSolventInteraction(uint32_t solventType, uint32_t monoType) const;

  void setContactInteraction(uint32_t typeA, uint32_t typeB,double energy);
  double getContactInteraction(uint32_t typeA,uint32_t typeB) const;

  //for all unknown moves: does nothing
  template < class IngredientsType> 
  bool checkMove( const IngredientsType& ingredients, const MoveBase& move ) const;

  //overload for MoveLocalSc
  template < class IngredientsType> 
  bool checkMove( const IngredientsType& ingredients, MoveLocalSc& move ) const;

  //overload for MoveAddMonomerSc
  template < class IngredientsType> 
  bool checkMove( const IngredientsType& ingredients, MoveAddMonomerSc& move ) const;
	
  //for unknown moves(does nothing)
  template<class IngredientsType> 
  void applyMove(IngredientsType& ing, const MoveBase& move);
	
  //for moves of type MoveLocalBase
  template<class IngredientsType> 
  void applyMove(IngredientsType& ing, const MoveLocalSc& move);

  //for moves of type InsertParticleMove
  template<class IngredientsType> 
  void applyMove(IngredientsType& ing, const MoveAddMonomerSc& move);
 
	
  //synchronize: calles fillLattice
  template<class IngredientsType>
  void synchronize(IngredientsType& ingredients);

private:
		
  //populate the lattice with the cooridinates from molecules
  //  template<class IngredientsType> void fillLattice(IngredientsType& ingredients);

  template<class IngredientsType> void syncSolvent(IngredientsType& ingredients);
  template<class IngredientsType> void syncPositions(IngredientsType& ingredients);
	
  //this is somehow dangerous, but I do it anyways
  SolventDistribution* solvent;

  template<class IngredientsType> bool isOccupied(const IngredientsType& ing,const VectorInt3& pos) const;

  template<class IngredientsType> void setOccupied(IngredientsType& ing,const VectorInt3& pos,uint32_t monoType);

  template<class IngredientsType> void setUnoccupied(IngredientsType& ing,const VectorInt3& pos);

  double getProbabilityFactor(uint32_t monoType,uint32_t latticeEntry) const;

  template<class IngredientsType>
    double calculateAcceptanceProb_plusX(const IngredientsType& ingredients,
				   const VectorInt3& currentPos,
				   uint32_t monoType) const;
				   

template<class IngredientsType>
    double calculateAcceptanceProb_minusX(const IngredientsType& ingredients,
				   const VectorInt3& currentPos,
				   uint32_t monoType) const;
				   
template<class IngredientsType>
    double calculateAcceptanceProb_plusY(const IngredientsType& ingredients,
				   const VectorInt3& currentPos,
				   uint32_t monoType) const;

template<class IngredientsType>
    double calculateAcceptanceProb_minusY(const IngredientsType& ingredients,
				   const VectorInt3& currentPos,
				   uint32_t monoType) const;

template<class IngredientsType>
    double calculateAcceptanceProb_plusZ(const IngredientsType& ingredients,
				   const VectorInt3& currentPos,
				   uint32_t monoType) const;

template<class IngredientsType>
    double calculateAcceptanceProb_minusZ(const IngredientsType& ingredients,
				   const VectorInt3& currentPos,
				   uint32_t monoType) const;
				   
  double probabilityLookup[7][256];
  double interactionEnergySolvent[7][4];
  double interactionEnergyContact[7][7];
  
  static const VectorInt3 interactionShell_plusX_on[24];
  static const VectorInt3 interactionShell_plusX_off[28];
  static const VectorInt3 interactionShell_minusX_on[24];
  static const VectorInt3 interactionShell_minusX_off[28];
  static const VectorInt3 interactionShell_plusY_on[24];
  static const VectorInt3 interactionShell_plusY_off[28];
  static const VectorInt3 interactionShell_minusY_on[24];
  static const VectorInt3 interactionShell_minusY_off[28];
  static const VectorInt3 interactionShell_plusZ_on[24];
  static const VectorInt3 interactionShell_plusZ_off[28];
  static const VectorInt3 interactionShell_minusZ_on[24];
  static const VectorInt3 interactionShell_minusZ_off[28];
};

///////////////////////////////////////////////////////////////////////////////
////////////////////////// member definitions /////////////////////////////////

/******************************************************************************/
/**
 * @brief returns true for all moves other than the ones that have specialized versions of this function.
 * */
/******************************************************************************/



template < class IngredientsType > 
bool FeatureImplicitSolventd2_X::checkMove( const IngredientsType& ingredients, 
						      const MoveBase& move )const
{
  return true;
}

/******************************************************************************/
/**
 * @brief checks  moves of type MoveLocalSc
 * */
/******************************************************************************/
template < class IngredientsType> 
bool FeatureImplicitSolventd2_X::checkMove( const IngredientsType& ingredients, 
					MoveLocalSc& move ) const
{

	//get the position of the monomer to be moved (assume "lower left corner")
	VectorInt3 currentPos=ingredients.getMolecules()[move.getIndex()];
	//get the direction of the move
	VectorInt3 direction=move.getDir();

	/*get two directions perpendicular to vector directon of the move*/
	VectorInt3 perp1,perp2;
  	/* first perpendicular direction is either (0 1 0) or (1 0 0)*/
	int32_t x1=((direction.getX()==0) ? 1 : 0);
	int32_t y1=((direction.getX()!=0) ? 1 : 0);
	perp1.setX(x1);
	perp1.setY(y1);
	perp1.setZ(0);
	
	/* second perpendicular direction is either (0 0 1) or (0 1 0)*/
	int32_t y2=((direction.getZ()==0) ? 0 : 1);
	int32_t z2=((direction.getZ()!=0) ? 0 : 1);
	perp2.setX(0);
	perp2.setY(y2);
	perp2.setZ(z2);

	
	//first check excluded volume
	VectorInt3 checkPos=currentPos;
	if(direction.getX()>0 || direction.getY()>0 || direction.getZ()>0) checkPos+=direction;
	checkPos+=direction; 
	
	if	(
		 (isOccupied(ingredients,checkPos))||
		 (isOccupied(ingredients,checkPos+perp1))||
		 (isOccupied(ingredients,checkPos+perp2))||
		 (isOccupied(ingredients,checkPos+perp1+perp2))
		 ) return false;

	//if excluded volume ok, check interactions
	else{
	  
	  uint32_t monoType=ingredients.getMolecules()[move.getIndex()].getAttributeTag();

	  double prob=1.0;
	  if(direction.getX()==1) prob=calculateAcceptanceProb_plusX(ingredients,currentPos,monoType);
	  else if(direction.getX()==-1) prob=calculateAcceptanceProb_minusX(ingredients,currentPos,monoType);
	  else if(direction.getY()==1) prob=calculateAcceptanceProb_plusY(ingredients,currentPos,monoType);
	  else if(direction.getY()==-1) prob=calculateAcceptanceProb_minusY(ingredients,currentPos,monoType);
	  else if(direction.getZ()==1) prob=calculateAcceptanceProb_plusZ(ingredients,currentPos,monoType);
	  else if(direction.getZ()==-1) prob=calculateAcceptanceProb_minusZ(ingredients,currentPos,monoType);
	  
	  move.multiplyProbability(prob);
					      
	  return true;
	}
	

}



/******************************************************************************/
/**
 * @fn bool FeatureImplicitSolventd2_X::checkMove( const IngredientsType& ingredients, const MoveAddMonomerSc& move )const
 * @brief checks excluded volume for insertion of a monomer
 * */
/******************************************************************************/

template < class IngredientsType>
bool FeatureImplicitSolventd2_X::checkMove( const IngredientsType& ingredients, MoveAddMonomerSc& move ) const
{

  //check if the lattice sites are free
  VectorInt3 pos=move.getPosition();
  VectorInt3 dx(1,0,0);
  VectorInt3 dy(0,1,0);
  VectorInt3 dz(0,0,1);

  //first check excluded volume
  if	(
	 (ingredients.isOccupied(ingredients,pos))||
	 (ingredients.isOccupied(ingredients,pos+dx))||
	 (ingredients.isOccupied(ingredients,pos+dy))||
	 (ingredients.isOccupied(ingredients,pos+dx+dy))||
	 (ingredients.isOccupied(ingredients,pos+dz))||
	 (ingredients.isOccupied(ingredients,pos+dz+dx))||
	 (ingredients.isOccupied(ingredients,pos+dz+dy))||
	 (ingredients.isOccupied(ingredients,pos+dz+dx+dy))
	 ) return false;

  //if excluded volume is ok, calculate acceptance probability
  else
    return true;
}


/******************************************************************************/
/**
 * @fn void FeatureImplicitSolventd2_X::applyMove(IngredientsType& ing, const Move& move)
 * @brief Reaction to unknown moves: do nothing
 * */
/******************************************************************************/

template<class IngredientsType> 
void FeatureImplicitSolventd2_X::applyMove(IngredientsType& ing, const MoveBase& move)
{
	
}

/******************************************************************************/
/**
 * @fn void FeatureImplicitSolventd2_X::applyMove(IngredientsType& ing, const MoveLocalBase<LocalMoveType>& move)
 * @brief updates the lattice ocupation according to the move (for moves of type MoveLocalBase)
 * */
/******************************************************************************/

template<class IngredientsType> 
void FeatureImplicitSolventd2_X::applyMove(IngredientsType& ing, const MoveLocalSc& move)
{
	//get old position and direction of the move
	VectorInt3 oldPos=ing.getMolecules()[move.getIndex()];
	VectorInt3 direction=move.getDir();	
	
	/*get two directions perpendicular to vector directon of the move*/
	VectorInt3 perp1,perp2;
  	/* first perpendicular direction is either (0 1 0) or (1 0 0)*/
	int32_t x1=((direction.getX()==0) ? 1 : 0);
	int32_t y1=((direction.getX()!=0) ? 1 : 0);
	perp1.setX(x1);
	perp1.setY(y1);
	perp1.setZ(0);
	
	/* second perpendicular direction is either (0 0 1) or (0 1 0)*/
	int32_t y2=((direction.getZ()==0) ? 0 : 1);
	int32_t z2=((direction.getZ()!=0) ? 0 : 1);
	perp2.setX(0);
	perp2.setY(y2);
	perp2.setZ(z2);
	
	VectorInt3 uncheckPos=oldPos;
	if(direction.getX()<0 || direction.getY()<0 || direction.getZ()<0) uncheckPos-=direction;

	VectorInt3 checkPos=uncheckPos+2*direction;
	
	//change lattice occupation accordingly
	//set Unoccupied and set occupied take care of solvent depletion.
	setUnoccupied(ing,uncheckPos);
	setUnoccupied(ing,uncheckPos+perp1);
	setUnoccupied(ing,uncheckPos+perp2);
	setUnoccupied(ing,uncheckPos+perp1+perp2);

	uint32_t monoType=ing.getMolecules()[move.getIndex()].getAttributeTag();
	setOccupied(ing,checkPos,monoType);
	setOccupied(ing,checkPos+perp1,monoType);
	setOccupied(ing,checkPos+perp2,monoType);
	setOccupied(ing,checkPos+perp1+perp2,monoType);
}


template<class IngredientsType> 
void FeatureImplicitSolventd2_X::applyMove(IngredientsType& ing, const MoveAddMonomerSc& move)
{
  VectorInt3 pos=move.getPosition();
  VectorInt3 dx(1,0,0);
  VectorInt3 dy(0,1,0);
  VectorInt3 dz(0,0,1);

  uint32_t monoType=ing.getMolecules()[move.getParticleIndex()].getAttributeTag();

  setOccupied(ing,pos,monoType);
  setOccupied(ing,pos+dx,monoType);
  setOccupied(ing,pos+dy,monoType);
  setOccupied(ing,pos+dx+dy,monoType);
  setOccupied(ing,pos+dz,monoType);
  setOccupied(ing,pos+dz+dx,monoType);
  setOccupied(ing,pos+dz+dy,monoType);
  setOccupied(ing,pos+dz+dx+dy,monoType);
}


template<class IngredientsType>
void FeatureImplicitSolventd2_X::synchronize(IngredientsType& ingredients)
{
  if(solvent==0){
    throw std::runtime_error("FeatureImplicitSolventd2_X: need to set solvent environment\n");
  }
  syncSolvent(ingredients);
  syncPositions(ingredients);
}
  
	
template<class IngredientsType> 
void FeatureImplicitSolventd2_X::syncSolvent(IngredientsType& ingredients)
{
  for(int32_t x=0;x<ingredients.getBoxX();x++)
    for(int32_t y=0;y<ingredients.getBoxY();y++)
      for(int32_t z=0;z<ingredients.getBoxZ();z++){
	
	uint8_t latticeEntry=ingredients.getLatticeEntry(x,y,z);
	//delete previous potential info, stored in first two bits
	latticeEntry &= (~3);

	//set new solvent info
	if(solvent->getTagAt(x,y,z)>3)
	  throw std::runtime_error("FeatureImplicitSolventd2_X: Tag for implicit solvent must be smaller 4\n");	
	latticeEntry |= (solvent->getTagAt(x,y,z) & 3);
	ingredients.setLatticeEntry(x,y,z,latticeEntry);
      }
  
}

template<class IngredientsType> 
void FeatureImplicitSolventd2_X::syncPositions(IngredientsType& ingredients)
{
  VectorInt3 dx(1,0,0);
  VectorInt3 dy(0,1,0);
  VectorInt3 dz(0,0,1);
			
  const typename IngredientsType::molecules_type& molecules=ingredients.getMolecules();
  //copy the lattice occupation from the monomer coordinates
  for(size_t n=0;n<molecules.size();n++)
    {
      
      if( isOccupied(ingredients,molecules[n]))
	{
	  throw std::runtime_error("FeatureImplicitSolventd2_X: multiple lattice occupation");
	}
      else
	{
	  VectorInt3 pos=molecules[n];
	  uint32_t monoType=ingredients.getMolecules()[n].getAttributeTag();
	  if(monoType>7) 
	  {
		  throw std::runtime_error("FeatureImplicitSolventd2_X: max attribute tag is 7");
		  
	  }

	  setOccupied(ingredients,pos,monoType);
	  setOccupied(ingredients,pos+dx,monoType);
	  setOccupied(ingredients,pos+dy,monoType);
	  setOccupied(ingredients,pos+dx+dy,monoType);
	  setOccupied(ingredients,pos+dz,monoType);
	  setOccupied(ingredients,pos+dz+dx,monoType);
	  setOccupied(ingredients,pos+dz+dy,monoType);
	  setOccupied(ingredients,pos+dz+dx+dy,monoType);
			
	}
				
    }
	
}





template<class IngredientsType>
double FeatureImplicitSolventd2_X::calculateAcceptanceProb_plusX(const IngredientsType& ingredients,
						       const VectorInt3& currentPos,
						       uint32_t monoType
      						) const
{
  	  
  double prob=1.0;
  
  for(size_t n=0;n<24;n++){
	  prob*=getProbabilityFactor(monoType,uint32_t(ingredients.getLatticeEntry(currentPos+interactionShell_plusX_on[n])));
  }

//   uint32_t latticeEntry;
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(0,1,0)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(0,1,1)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(0,0,1)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
  
  double prob_div=1.0;
  for(size_t n=0;n<28;n++){
	  prob_div*=getProbabilityFactor(monoType,uint32_t(ingredients.getLatticeEntry(currentPos+interactionShell_plusX_off[n])));
  }

  
  prob/=prob_div;
  
  return prob;
    
}

template<class IngredientsType>
double FeatureImplicitSolventd2_X::calculateAcceptanceProb_minusX(const IngredientsType& ingredients,
						       const VectorInt3& currentPos,
						       uint32_t monoType
      						) const
{
  	  
  double prob=1.0;
  
  for(size_t n=0;n<24;n++){
	  prob*=getProbabilityFactor(monoType,uint32_t(ingredients.getLatticeEntry(currentPos+interactionShell_minusX_on[n])));
  }

//   uint32_t latticeEntry;
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(1,0,0)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(1,1,0)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(1,1,1)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(1,0,1)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   
  
  double prob_div=1.0;
  for(size_t n=0;n<28;n++){
	  prob_div*=getProbabilityFactor(monoType,uint32_t(ingredients.getLatticeEntry(currentPos+interactionShell_minusX_off[n])));
  }
  
  prob/=prob_div;
  
  return prob;
    
}

template<class IngredientsType>
double FeatureImplicitSolventd2_X::calculateAcceptanceProb_plusY(const IngredientsType& ingredients,
						       const VectorInt3& currentPos,
						       uint32_t monoType
      						) const
{
  	  
  double prob=1.0;
  
  for(size_t n=0;n<24;n++){
	  prob*=getProbabilityFactor(monoType,uint32_t(ingredients.getLatticeEntry(currentPos+interactionShell_plusY_on[n])));
  }

//   uint32_t latticeEntry;
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(1,0,0)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(1,0,1)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(0,0,1)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   
  double prob_div=1.0;
  for(size_t n=0;n<28;n++){
	  prob_div*=getProbabilityFactor(monoType,uint32_t(ingredients.getLatticeEntry(currentPos+interactionShell_plusY_off[n])));
  }

  
  prob/=prob_div;
  
  return prob;
    
}

template<class IngredientsType>
double FeatureImplicitSolventd2_X::calculateAcceptanceProb_minusY(const IngredientsType& ingredients,
						       const VectorInt3& currentPos,
						       uint32_t monoType
      						) const
{
  	  
  double prob=1.0;
  
  for(size_t n=0;n<24;n++){
	  prob*=getProbabilityFactor(monoType,uint32_t(ingredients.getLatticeEntry(currentPos+interactionShell_minusY_on[n])));
  }

//   uint32_t latticeEntry;
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(0,1,0)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(1,1,0)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(1,1,1)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(0,1,1)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
  
  
  double prob_div=1.0;
  for(size_t n=0;n<28;n++){
	  prob_div*=getProbabilityFactor(monoType,uint32_t(ingredients.getLatticeEntry(currentPos+interactionShell_minusY_off[n])));
  }
  
  prob/=prob_div;
  
  return prob;
    
}

template<class IngredientsType>
double FeatureImplicitSolventd2_X::calculateAcceptanceProb_plusZ(const IngredientsType& ingredients,
						       const VectorInt3& currentPos,
						       uint32_t monoType
      						) const
{
  	  
  double prob=1.0;
  
  for(size_t n=0;n<24;n++){
	  prob*=getProbabilityFactor(monoType,uint32_t(ingredients.getLatticeEntry(currentPos+interactionShell_plusZ_on[n])));
  }

//   uint32_t latticeEntry;
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(1,0,0)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(1,1,0)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(0,1,0)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
  
  double prob_div=1.0;
  for(size_t n=0;n<28;n++){
	  prob_div*=getProbabilityFactor(monoType,uint32_t(ingredients.getLatticeEntry(currentPos+interactionShell_plusZ_off[n])));
  }

  
  prob/=prob_div;
  
  return prob;
    
}

template<class IngredientsType>
double FeatureImplicitSolventd2_X::calculateAcceptanceProb_minusZ(const IngredientsType& ingredients,
						       const VectorInt3& currentPos,
						       uint32_t monoType
      						) const
{
  	  
  double prob=1.0;
  
  for(size_t n=0;n<24;n++){
	  prob*=getProbabilityFactor(monoType,uint32_t(ingredients.getLatticeEntry(currentPos+interactionShell_minusZ_on[n])));
  }

//   uint32_t latticeEntry;
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(0,0,1)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(1,0,1)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(1,1,1)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
//   latticeEntry=uint32_t(ingredients.getLatticeEntry(currentPos+VectorInt3(0,1,1)) & 3);
//   prob*=getProbabilityFactor(monoType,latticeEntry);
  
  double prob_div=1.0;
  for(size_t n=0;n<28;n++){
	  prob_div*=getProbabilityFactor(monoType,uint32_t(ingredients.getLatticeEntry(currentPos+interactionShell_minusZ_off[n])));
  }

  
  prob/=prob_div;
  
  return prob;
    
}

template<class IngredientsType> 
void FeatureImplicitSolventd2_X::setOccupied(IngredientsType& ing,const VectorInt3& pos,uint32_t monoType)
{
  uint8_t latticeEntry=ing.getLatticeEntry(pos);
  uint8_t attributeTag(monoType);
  latticeEntry+=attributeTag<<5;
  ing.setLatticeEntry(pos,latticeEntry);
  
  //now deplete the solvent around pos
  latticeEntry=ing.getLatticeEntry(pos+VectorInt3(1,0,0));
  latticeEntry+=4;
  ing.setLatticeEntry(pos+VectorInt3(1,0,0),latticeEntry);
  
  latticeEntry=ing.getLatticeEntry(pos+VectorInt3(-1,0,0));
  latticeEntry+=4;
  ing.setLatticeEntry(pos+VectorInt3(-1,0,0),latticeEntry);
  
  latticeEntry=ing.getLatticeEntry(pos+VectorInt3(0,1,0));
  latticeEntry+=4;
  ing.setLatticeEntry(pos+VectorInt3(0,1,0),latticeEntry);
  
  latticeEntry=ing.getLatticeEntry(pos+VectorInt3(0,-1,0));
  latticeEntry+=4;
  ing.setLatticeEntry(pos+VectorInt3(0,-1,0),latticeEntry);
  
  latticeEntry=ing.getLatticeEntry(pos+VectorInt3(0,0,1));
  latticeEntry+=4;
  ing.setLatticeEntry(pos+VectorInt3(0,0,1),latticeEntry);
  
  latticeEntry=ing.getLatticeEntry(pos+VectorInt3(0,0,-1));
  latticeEntry+=4;
  ing.setLatticeEntry(pos+VectorInt3(0,0,-1),latticeEntry);
}

template<class IngredientsType> 
void FeatureImplicitSolventd2_X::setUnoccupied(IngredientsType& ing,const VectorInt3& pos)
{
  uint8_t latticeEntry=ing.getLatticeEntry(pos);
  latticeEntry&=31;
  ing.setLatticeEntry(pos,latticeEntry);
  
  //now remove solvent depletion
  latticeEntry=ing.getLatticeEntry(pos+VectorInt3(1,0,0));
  latticeEntry-=4;
  ing.setLatticeEntry(pos+VectorInt3(1,0,0),latticeEntry);
  
  latticeEntry=ing.getLatticeEntry(pos+VectorInt3(-1,0,0));
  latticeEntry-=4;
  ing.setLatticeEntry(pos+VectorInt3(-1,0,0),latticeEntry);
  
  latticeEntry=ing.getLatticeEntry(pos+VectorInt3(0,1,0));
  latticeEntry-=4;
  ing.setLatticeEntry(pos+VectorInt3(0,1,0),latticeEntry);
  
  latticeEntry=ing.getLatticeEntry(pos+VectorInt3(0,-1,0));
  latticeEntry-=4;
  ing.setLatticeEntry(pos+VectorInt3(0,-1,0),latticeEntry);
  
  latticeEntry=ing.getLatticeEntry(pos+VectorInt3(0,0,1));
  latticeEntry-=4;
  ing.setLatticeEntry(pos+VectorInt3(0,0,1),latticeEntry);
  
  latticeEntry=ing.getLatticeEntry(pos+VectorInt3(0,0,-1));
  latticeEntry-=4;
  ing.setLatticeEntry(pos+VectorInt3(0,0,-1),latticeEntry);
}

template<class IngredientsType> 
bool FeatureImplicitSolventd2_X::isOccupied(const IngredientsType& ing,const VectorInt3& pos) const
{
  uint8_t latticeEntry=ing.getLatticeEntry(pos);
  if( (latticeEntry>>5)>0)
    return true;
  else 
    return false;
}




#endif

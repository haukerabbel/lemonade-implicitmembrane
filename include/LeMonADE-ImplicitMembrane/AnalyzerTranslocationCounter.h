#ifndef ANALYZER_TRANSLOCATION_COUNTER_H
#define ANALYZER_TRANSLOCATION_COUNTER_H

#include <LeMonADE/analyzer/AbstractAnalyzer.h>
#include <LeMonADE/utility/ResultFormattingTools.h>
#include <LeMonADE/utility/Vector3D.h>

/************************************************************************
 * calculates the probability histogram of the coordinates of a group of
 * particles in a box
 * **********************************************************************/


template < class IngredientsType > class AnalyzerTranslocationCounter : public AbstractAnalyzer
{
	typedef typename IngredientsType::molecules_type molecules_type;
	
	const IngredientsType& ingredients;
	
	const std::vector<MonomerGroup<molecules_type> >& groups;
	
	std::vector<double> translocations;
	std::vector<int32_t> previousZone;
	std::vector<std::vector<int32_t> > zoneHistory;
	
	double barrier1,barrier2;
	
	double foldBack(double value, int32_t box) const {
			
		//make coordinate positive, so that modulo is properly defined
		while (value < 0.0) {
			value += double(box);
		}
		while (value >= double(box)) {
			value -= double(box);
		}
		return value;
	}
	
	int32_t getCurrentZone(double coordinate){
		if(coordinate<barrier1) return 1;
		else if(coordinate<barrier2) return 2;
		else return 3;
	}
	
	template < class MoleculesType > VectorDouble3 centerOfMass(const MoleculesType& m) const
	{
		VectorDouble3 CoM_sum;    
		for ( uint i = 0; i < m.size(); ++i)
		{
			CoM_sum.setX( CoM_sum.getX() + m[i].getX() );
			CoM_sum.setY( CoM_sum.getY() + m[i].getY() );
			CoM_sum.setZ( CoM_sum.getZ() + m[i].getZ() );
		}
		
		double inv_N = 1.0 / double ( m.size() );
		
		VectorDouble3 CoM (
			double ( CoM_sum.getX() ) * inv_N, 
				   double ( CoM_sum.getY() ) * inv_N,
				   double ( CoM_sum.getZ() ) * inv_N);
		
		return CoM;
		
	}
	
	
public:
  
	AnalyzerTranslocationCounter(const IngredientsType& ing, const std::vector<MonomerGroup<molecules_type> >& g,double b1,double b2);
	
	virtual ~AnalyzerTranslocationCounter(){}
	
	virtual void initialize();
	virtual bool execute();
	virtual void cleanup();

};

/*************************************************************************
 * implementation of memeber execute()
 * ***********************************************************************/


template<class IngredientsType>
AnalyzerTranslocationCounter<IngredientsType>::AnalyzerTranslocationCounter(const IngredientsType& ing, const std::vector<MonomerGroup<molecules_type> >& g,double b1=-1.0,double b2=-1.0)
:ingredients(ing),barrier1(b1),barrier2(b2),groups(g)
{
	
}


template< class IngredientsType>
void AnalyzerTranslocationCounter<IngredientsType>::initialize()
{
	
	if(barrier1<0.0 || barrier2<0.0){
		barrier1=double(ingredients.getBoxZ())/2.0-14.0;
		barrier2=double(ingredients.getBoxZ())/2.0+14.0;
	}
	
	translocations.resize(groups.size(),0.0);
	previousZone.resize(groups.size(),0);
	zoneHistory.resize(groups.size());
	for(size_t n=0;n<zoneHistory.size();n++){
		zoneHistory[n].resize(3,0);
	}
}

template< class IngredientsType >
bool AnalyzerTranslocationCounter<IngredientsType>::execute()
{
	
	for(size_t n=0;n<groups.size();n++)
	{
		VectorDouble3 pos=centerOfMass(groups[n]);
		
		int32_t zone=getCurrentZone( foldBack(pos.getZ(),ingredients.getBoxZ()));
		if(zone!=previousZone[n]){
			zoneHistory[n].push_back(zone);
			zoneHistory[n].erase(zoneHistory[n].begin());
			previousZone[n]=zone;
			if(zoneHistory[n][1]==2){
				if(zoneHistory[n][0]==1 && zoneHistory[n][2]==3) translocations[n]+=1.0;
				else if(zoneHistory[n][0]==3 && zoneHistory[n][2]==1) translocations[n]+=1.0;
			}
		}
	}
	
	return true;
}

template<class IngredientsType>
void AnalyzerTranslocationCounter<IngredientsType>::cleanup()
{
	
	//now write to file (including a comment)
	std::stringstream comment;
	comment<<"PolymerTranslocations.\n";
	comment<<"sample size: "<<groups.size()<<" groups\n";

	std::vector<std::vector<double> > trans;
	trans.push_back(translocations);
	
	
	
	ResultFormattingTools::writeResultFile("Translocations.dat",ingredients,trans,comment.str());
	
	
	
}




#endif



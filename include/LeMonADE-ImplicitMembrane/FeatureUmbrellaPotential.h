#ifndef FEATURE_GROUP_POTENTIAL_H
#define FEATURE_GROUP_POTENTIAL_H

#include <LeMonADE/updater/moves/MoveBase.h>
#include <LeMonADE/updater/moves/MoveLocalSc.h>
#include <LeMonADE/utility/DistanceCalculation.h>
#include <LeMonADE/feature/Feature.h>
#include <LeMonADE/feature/FeatureBoltzmann.h>
#include <LeMonADE/feature/FeatureAttributes.h>

#include <LeMonADE-Extensions/utilities/Histogram1D.h>


class FeatureUmbrellaPotential:public Feature
{
public:
	typedef LOKI_TYPELIST_1(FeatureBoltzmann) required_features_back;
		
	FeatureUmbrellaPotential()
	:allMonomersAffected(true)
	,previousMonomerGroupCOM_Z(0.0)
	,newMonomerGroupCOM_Z(0.0){}
	
	virtual ~FeatureUmbrellaPotential(){}
    
        //set the monomers affected
	void setAffectedMonomers(std::set<size_t> m){allMonomersAffected=false; affectedMonomers=m;}
	void setAffectedMonomersAll(bool flag){allMonomersAffected=flag;}
	
	//for all unknown moves: does nothing
	template < class IngredientsType> 
	bool checkMove( const IngredientsType& ingredients, const MoveBase& move ) const;

	//overload for MoveLocalSc
	template < class IngredientsType> 
	bool checkMove( const IngredientsType& ingredients, MoveLocalSc& move );

	//for unknown moves(does nothing)
	template<class IngredientsType> 
	void applyMove(IngredientsType& ing, const MoveBase& move){}
	
	//for moves of type MoveLocalSc
	template<class IngredientsType> 
	void applyMove(IngredientsType& ing, const MoveLocalSc& move);

	
	template<class IngredientsType>
	void synchronize(IngredientsType& ingredients);
	
	
	//get the current pmf
	Histogram1D& modifyUmbrellaPotential() {return umbrellaPotential;}
	const Histogram1D& getUmbrellaPotential() const {return umbrellaPotential;} 
	
	double getCurrentReactionCoordinate() const {return previousMonomerGroupCOM_Z;}
	
	
private:
	
	//gives the z-coordinate of the center of mass of the affected group of monomers
	//the extra parameters are there, so that one can use the function inside checkMove,
	//where a single monomer with index "index" is hypothetically displaced by a vector "direction"
	template<class IngredientsType>
	double getUpdatedGroupCOM_Z(const IngredientsType& ingredients,size_t index=0,VectorInt3 direction=VectorInt3(0,0,0)) const;
	
	//contains the indices of the monomers with type affectedMonomerType
	bool allMonomersAffected;
	std::set<size_t> affectedMonomers;
	
	Histogram1D umbrellaPotential;
	
	double previousMonomerGroupCOM_Z;
	double newMonomerGroupCOM_Z;
	
};



///////////////////////////////////////////////////////////////////////////////
////////////////////Implementation of methods////////////////////////////


template<class IngredientsType>
bool FeatureUmbrellaPotential::checkMove(const IngredientsType& ingredients, const MoveBase& move) const
{
	return true;//nothing to do for unknown moves
}



template<class IngredientsType>
bool FeatureUmbrellaPotential::checkMove(const IngredientsType& ingredients, MoveLocalSc& move)
{
	
	if(!allMonomersAffected){
		if(affectedMonomers.count(move.getIndex())==0) return true;
	}
	
	newMonomerGroupCOM_Z=getUpdatedGroupCOM_Z(ingredients,move.getIndex(),move.getDir());
	
	//fold the bin inside the box
	while (newMonomerGroupCOM_Z < 0.0) {
		newMonomerGroupCOM_Z += ingredients.getBoxZ();
	}
	while (newMonomerGroupCOM_Z >= ingredients.getBoxZ()) {
		newMonomerGroupCOM_Z -= ingredients.getBoxZ();
	}
	
	
	//add move probability according to current potentialOfMeanForce
	move.multiplyProbability(std::exp(-(umbrellaPotential.getCountAt(newMonomerGroupCOM_Z)-umbrellaPotential.getCountAt(previousMonomerGroupCOM_Z))));	
	
	
	return true;
}


template<class IngredientsType>
void FeatureUmbrellaPotential::applyMove(IngredientsType& ingredients, const MoveLocalSc& move)
{
	//update the book keeping variables in case the move was accepted
	previousMonomerGroupCOM_Z=newMonomerGroupCOM_Z;
	
}


template<class IngredientsType>
void FeatureUmbrellaPotential::synchronize(IngredientsType& ingredients)
{
	
}


//calculate the center of mass of the affected monomer group. if index
//and direction are given, a hypothetical displaced of monmer "index" by
//a vector "direction" is assumed.
template<class IngredientsType>
double FeatureUmbrellaPotential::getUpdatedGroupCOM_Z(const IngredientsType& ingredients,size_t index, VectorInt3 direction) const
{
	//the default for index is 0, the default for direction is 0,0,0. the index 0 points to a particle,
	//but since one has to explicitly specify index if one changes direction to anything other than 0,0,0
	//there is no danger in using the function without explicit arguments
	
	int32_t sumZ=0;
	double counter=0.0;
	for(size_t n=0;n<ingredients.getMolecules().size();n++)
	{
		if(allMonomersAffected || affectedMonomers.count(n)==1){
			counter+=1.0;
			if(n==index)
				sumZ+=ingredients.getMolecules()[n].getZ()+direction.getZ();
		
			else 
				sumZ+=ingredients.getMolecules()[n].getZ();
		}
	}
	return double(sumZ)/counter;
}

#endif //FEATURE_METADYNAMICS_FIXED_POTENTIAL_H

#ifndef IMPLICIT_MEMBRANE_POTENTIALS_H
#define IMPLICIT_MEMBRANE_POTENTIALS_H

#include<stdexcept>
#include <limits>
#include <cmath>




class SquarePotential
{
public:
	SquarePotential():depth(1.0),width(1),borderLow(0),borderHigh(0),boxZm1(0){}
	~SquarePotential(){}
	
    double operator()(const VectorInt3& position,int32_t attribute)
	{
	    int32_t z=position.getZ();
	    while (z<0) z+=(boxZm1+1);
	    z &= boxZm1;    
	    if(z >= borderLow && z<=borderHigh) return depth;
	    else return 0.0;
	
	}
	
    template<class IngredientsType> void setup(const IngredientsType& ing)
	{
	    int32_t boxZ=ing.getBoxZ();
	    boxZm1=boxZ-1;
	    borderLow=boxZ/2-width;
	    borderHigh=boxZ/2+width;
	    
	}

    void setDepth(double d){depth=d;}
    void setWidth(double w){width=w;}
    	
private:
    
    double depth;
    double width;
    int32_t boxZm1; 
    int32_t borderHigh,borderLow;
};



class ImplicitMembranePotential
{
public:
    ImplicitMembranePotential():depthD(0.0),depthH(0.0),widthD(0.0),widthH(0.0)
	{
	    hydrophobicity.resize(4,0.0);
	}
    ~ImplicitMembranePotential(){}
	
    double operator()(const VectorInt3& position,int32_t attribute)
	{
	    int32_t z=position.getZ();
	    while (z<0) z+=(boxZm1+1);
	    z &= boxZm1;
	    double retVal=0.0;
	    if(z >= densityLow && z<densityHigh) retVal+=depthD;

	    if(z >= hydrophobLow && z<hydrophobHigh) retVal+=(1-hydrophobicity[attribute-1])*depthH;
	    else retVal+=hydrophobicity[attribute-1]*depthH;
	    
	    return retVal;
	}
	
    template<class IngredientsType> void setup(const IngredientsType& ing)
	{
	    int32_t boxZ=ing.getBoxZ();
	    boxZm1=boxZ-1;
	    densityLow=boxZ/2-widthD;
	    densityHigh=boxZ/2+widthD;
	    hydrophobLow=boxZ/2-widthH;
	    hydrophobHigh=boxZ/2+widthH;
	    
	    for(size_t n=0;n<ing.getMolecules().size();n++)
	    {
		if(ing.getMolecules()[n].getAttributeTag()>4){
		    throw std::runtime_error("ImplicitMembranePotential: attribute larger 4 encountered.");
		}
		
	    }
	}

    void setDepthD(double d){depthD=d;}
    void setWidthD(int32_t w){widthD=w;}
    void setDepthH(double d){depthH=d;}
    void setWidthH(int32_t w){widthH=w;}
    void setHydrophobicities(std::vector<double> h){
	if(h.size()!=4) throw std::runtime_error("ImplicitMembranePotential::setHydrophobicities(). Argument must have length 4\n");
	hydrophobicity=h;
    }
	
private:
    
    double depthD;
    int32_t widthD;
    double depthH;
    int32_t widthH;
    int32_t boxZm1;    
    int32_t densityLow,densityHigh;
    int32_t hydrophobLow,hydrophobHigh;
    
    std::vector<double> hydrophobicity; 
        
};

class ChargedImplicitMembranePotential
{
public:
    ChargedImplicitMembranePotential():depthD(0.0),depthH(0.0),widthD(0.0),widthH(0.0),widthQ(0.0),depthQ(0.0),centerQ(0.0)
	{
	    hydrophobicity.resize(4,0.0);
	}
    ~ChargedImplicitMembranePotential(){}
	
    double operator()(const VectorInt3& position,int32_t attribute)
	{
	    int32_t z=position.getZ();
	    
	    while (z<0) z+=(boxZm1+1);
	    z &= boxZm1;
	    double retVal=0.0;
	    if(z >= densityLow && z<densityHigh) retVal+=depthD;

	    if(z >= hydrophobLow && z<hydrophobHigh) retVal+=(1-hydrophobicity[attribute-1])*depthH;
	    else retVal+=hydrophobicity[attribute-1]*depthH;
	    
	    //charge=+1
	    double q=0;
	    if(attribute==1) q=1.0;
	    else if (attribute==2) q=-1.0;
	    
	    //these must be two separate if statements, because the contributions
	    //from the opposing sites are addititve
	    if(z >= chargeCenterLow-widthQ && z<-chargeCenterLow+widthQ) retVal-=q*depthQ;
	    if(z >= chargeCenterHigh-widthQ && z<chargeCenterHigh+widthQ) retVal-=q*depthQ;
	    
	    return retVal;
	}
	
    template<class IngredientsType> void setup(const IngredientsType& ing)
	{
	    int32_t boxZ=ing.getBoxZ();
	    boxZm1=boxZ-1;
	    densityLow=boxZ/2-widthD;
	    densityHigh=boxZ/2+widthD;
	    hydrophobLow=boxZ/2-widthH;
	    hydrophobHigh=boxZ/2+widthH;
	    chargeCenterHigh=boxZ/2+centerQ;
	    chargeCenterHigh=boxZ/2-centerQ;

	    for(size_t n=0;n<ing.getMolecules().size();n++)
	    {
		if(ing.getMolecules()[n].getAttributeTag()>4){
		    throw std::runtime_error("ImplicitMembranePotential: attribute larger 4 encountered.");
		}
		
	    }
	}

    void setDepthD(double d){depthD=d;}
    void setWidthD(int32_t w){widthD=w;}
    void setDepthH(double d){depthH=d;}
    void setWidthH(int32_t w){widthH=w;}
    void setDepthQ(double d){depthQ=d;}
    void setWidthQ(int32_t w){widthQ=w;}
    void setCenterQ(int32_t c){centerQ=c;}
    
    void setHydrophobicities(std::vector<double> h){
	if(h.size()!=4) throw std::runtime_error("ImplicitMembranePotential::setHydrophobicities(). Argument must have length 4\n");
	hydrophobicity=h;
    }
	
private:
    
    double depthD;
    int32_t widthD;
    double depthH;
    int32_t widthH;
    double depthQ;
    int32_t widthQ;
    int32_t centerQ;
    
    
    int32_t boxZm1;    
    int32_t densityLow,densityHigh;
    int32_t hydrophobLow,hydrophobHigh;
    int32_t chargeCenterHigh,chargeCenterLow;

    std::vector<double> hydrophobicity; 
        
};
#endif 

#ifndef ADAPTIVE_UMBRELLA_UPDATER_H
#define ADAPTIVE_UMBRELLA_UPDATER_H

#include <limits>

#include <LeMonADE/updater/AbstractUpdater.h>

#include <LeMonADE-Extensions/utilities/Histogram1D.h>

/* ****************************************************************************
 * Simulator for adaptive umbrella sampling along a (1D) reaction coordinate.
 * The simulator runs standart BFM, while during  the simulation recording a 
 * histogram of the reaction coordinate. 
 * 
 * When the histogram has converged by three given criteria (described below),
 * a bias potential is constructed from the histogram, the histogram is 
 * reset to zero, and the procedure is repeated with the bias potential. 
 * This procedure is carried out with updates on the bias potential based on 
 * collected histograms, until the histogram becomes flat. Once this is the case
 * the bias potential is the inverted free energy profile along the coordinate.
 * 
 * The coordinate itsself is not specified in this updater. It requires 
 * Ingredients to have an interface with the signature 
 * double getCurrentReactionCoordinate()
 * Such an interface is provided by the FeatureBilayerDistancePotential. In that
 * case the coordinate is the distance to the bilayer. Other features could be 
 * used, provided they have the above described interface.
 * 
 * For the convergence of the histogram, three criteria are used to judge whether
 * the histogram has (locally) converged, i.e. does not change much any more.
 * These criteria are:
 * 1) relative change of the mean value of the expectation value of the coordinate.
 * <x>=sum(histogram(x) * x).
 * 2) relative change of the variance of the histogram coordinate
 * <dx**2>=<x**2>-<x>**2
 * 3) relative change of the square deviation from the mean of the histogram
 * itsself   <dH**2>=< (h(x) - <h>)**2 >
 * For these checks, the normalized histogram is used, and it is checked whether
 * these values changed in the last two update trials by more than some threshold
 * percentage.
 * 
 * For the absolute convergence, the square deviation of the histogram from the
 * mean is required to be less than some absolute value ( e.g. 0.1)
 * 
 * The threshold values can be set by the appropriate interfaces or the 
 * constructor.
 * ****************************************************************************/

/**
 * @file
 *
 * @class UpdaterAdaptiveUmbrellaSampling
 *
 * @brief Adaptive Umbrella Sampling updater along a reaction coordinate
 *
 * @details requires FeatureBilayerDistancePotential or other feature handling a 
 * possibly discrete potential along a reaction coordinate. this feature needs to
 * provide the functions: get/modifyPotential()  returning the potential object, 
 * getCurrentReactionCoordinate() returning the current value of the reaction
 * coordinate. also, the potential object needs to provide the functions 
 * addValue(coordinate, value) and getPMFMaxCoordinate(),getPMFMinCoordinate(),
 * getPMFValues() returning a std::vector<double> of the pmf values
 *
 * @tparam IngredientsType Ingredients class storing all system information( e.g. monomers, bonds, etc).
 * @tparam <MoveType> name of the specialized move.
 */
template<class IngredientsType,class MoveType>
class UpdaterAdaptiveUmbrellaSampling:public AbstractUpdater
{
	
public:
	/**
	 * @brief Standard Constructor initialized with ref to Ingredients and MCS per cycle
	 *
	 * @param ing a reference to the IngredientsType - mainly the system
	 * @param steps MCS per cycle to performed by execute()
	 * @param stepsBeforeBiasCheck interval between checks if histogram has converged im MCS
	 * @param stepsBeforeHistogramUpdate interval between updates on coordinate histogram in MCS
	 * @param convergenceThreshold threshold for relative convergence of histogram
	 * @param fullFlatnessThreshold threshold for absolute convergence of histogram
	 * @param maxAge maximum MCS limit. If the histogram converges earlier, the simulation also stops
	 */
	UpdaterAdaptiveUmbrellaSampling(IngredientsType& ing
	,uint32_t steps,uint32_t stepsBeforeBiasCheck
	,uint32_t stepsBeforeHistogramUpdate=1
	,double convergenceThreshold=0.02,double fullFlatnessThreshold=0.1
	,double binCountThreshold=10000.0
	,uint64_t maxAge=100000000);
	
	/**
	 * @brief This checks all used Feature and applies all Feature if all conditions are met.
	 *
	 * @details This function runs over \a steps MCS and performs the moves.
	 * It setting the age of the system and prints a simple simple simulation speed
	 * in the number of attempted monomer moves per s (tried and performed monomer moves/s).
	 *
	 * @return True if function is done.
	 */
	bool execute();
	
	
	/**
	 * @brief This function is called \a once in the beginning of the TaskManager.
	 *
	 * @details It´s a virtual function for inheritance.
	 * Use this function for initializing tasks (e.g. init SDL)
	 *
	 **/
	virtual void initialize();
	
	/**
	 * @brief This function is called \a once in the end of the TaskManager.
	 *
	 * @details writes the final state of histogram, bias potential and the
	 * history of the convergence criteria to output files.
	 *
	 **/
	virtual void cleanup(){
		dumpHistogram("final_histogram.dat");
		updateBiasPotential();
		dumpPMF();
		dumpConvergenceProgress();
	};
	
	
	//! set the threshold for relative convergence of histogram (for update of bias)
	void setRelativeConvergenceThreshold(double threshold){convergenceThreshold=threshold;}
	
	//! return the threshold for relative convergence of histogram (for update of bias)
	double getRelativeConvergenceThreshold() const{return convergenceThreshold;}
	
	//! set the threshold for complete convergence of histogram (for final simulation convergence)
	void setFullConvergenceThreshold(double threshold){fullFlatnessThreshold=threshold;}
	
	//! return the threshold for complete convergence of histogram (for final simulation convergence)
	void getFullConvergenceThreshold() const {return fullFlatnessThreshold;}
	
	//! get the number of pmf updates performed up to now on the bias potential
	uint32_t getNPerformedUpdates()const{return nUpdatesPerformed;}
	
private:
	//! write the currently collected histogram to a file named histogram_mcs.dat
	void dumpHistogram(std::string filePrefix);
	
	//! write the currently used bias potential, i.e. PMF estimate to a file named pmf_mcs.dat
	void dumpPMF();
	
	//! writes the time development of the mean,variance, and square deviation from average to file names convergence.dat
	void dumpConvergenceProgress();
	
	//! update the used bias potential according to collected histogram
	void updateBiasPotential();
	
	//! check if the histogram has converged according to chosen criteria
	bool histogramConverged();
	
	//! reset the histogram to empty state
	void resetHistogram();
	
	//! check the mean square deviation from a flat histogram to check for full conversion of the simulation
	double histogramFlattness() const;
	
	//! check if mean and variance of histogram have converged. called as part of histogramConverged()
	bool distributionCheck();
	
	//! check if histogram flatness has converged, called as part of histogramConverged()
	bool flatnessCheck();
	
	//! set bin width of coordinate histogram and external potential
	bool setBinWidth();
	
	
	//! System information
	IngredientsType& ingredients;
	
	//! Specialized move to be used
	MoveType move;
	
	//! Max number of mcs to be executed
	uint32_t nsteps;
	
	//! Number of steps between checks for update of bias
	const uint32_t nStepsBeforeBiasCheck;
	
	//! Number of checks before update of histogram
	const uint32_t nStepsBeforeHistogramUpdate;
	
	//! current counter to decide if update check is needed
	uint32_t counter_nStepsBeforeBiasCheck;
	
	//! current counter to decide if update check is needed
	uint32_t counter_nStepsBeforeHistogramUpdate;
	
	//! counts the number of bias updates performed so far
	uint32_t nUpdatesPerformed;
	
	//! histogram of reaction coordinate
	Histogram1D histogram;
	
	//values for checking convergence
	double oldMean,oldOldMean;
	double oldVariance,oldOldVariance;
	double oldFlatnesValue,oldOldFlatnessValue;
	
	//thresholds for convergence
	double convergenceThreshold;
	double fullFlatnessThreshold; //for flatness
	double maxBinsThreshold;
	
	//time history of convergence criteria
	std::vector<double> varianceSeries;
	std::vector<double> meanSeries;
	std::vector<double> flatnessSeries;
	
	//! set to true if full convergence threshold has been reached
	bool simulationConverged;
	
	//! flags for setting or unsetting extra output of histogram
	bool writeHistogramProgress;
	
	//! flags for setting or unsetting extra output of bias potential
	bool writePMFProgress;
	
	//! maximum age of the system
	uint64_t maxSystemAge;
	
};


////////////////////////////////////////////////////////////////////////////////
// member implementations
///////////////////////////////////////////////////////////////////////////////


// constructor
template<class IngredientsType,class MoveType>
UpdaterAdaptiveUmbrellaSampling<IngredientsType,MoveType>::
UpdaterAdaptiveUmbrellaSampling(IngredientsType& ing, 
				uint32_t steps, 
				uint32_t stepsBeforeBiasCheck,
				uint32_t stepsBeforeHistogramUpdate,
				double threshold,
				double fullThreshold,
				double binCountThreshold,
				uint64_t maxAge)
:ingredients(ing)
,nsteps(steps)
,counter_nStepsBeforeBiasCheck(0)
,counter_nStepsBeforeHistogramUpdate(0)
,nStepsBeforeBiasCheck(stepsBeforeBiasCheck)
,nStepsBeforeHistogramUpdate(stepsBeforeHistogramUpdate)
,writeHistogramProgress(true)
,writePMFProgress(true)
,nUpdatesPerformed(0)
,oldMean(0.0)
,oldOldMean(0.0)
,oldVariance(0.0)
,oldOldVariance(0.0)
,oldFlatnesValue(10.0)
,oldOldFlatnessValue(10.0)
,convergenceThreshold(threshold)
,fullFlatnessThreshold(fullThreshold)
,maxBinsThreshold(binCountThreshold)
,maxSystemAge(maxAge)
,simulationConverged(false)
{
	varianceSeries.resize(0);
	meanSeries.resize(0);
	flatnessSeries.resize(0);
}
 
 
//execute simulation
template<class IngredientsType, class MoveType>
bool UpdaterAdaptiveUmbrellaSampling<IngredientsType,MoveType>::execute()
{
	//some output for simulation performance and state
	time_t startTimer = std::time(NULL); //in seconds
	uint64_t systemAge=ingredients.getMolecules().getAge();
	std::cout<<"mcs "<<systemAge 
		 << " passed time " << ((std::difftime(std::time(NULL), startTimer)) ) <<std::endl;
	
	//if max age has reached, stop
	if(systemAge>=maxSystemAge) {
		std::cout<<"max age has been reached. maxAge "<<maxSystemAge<<" system age "<<systemAge<<std::endl;
		return false;
	}
	
	if(simulationConverged) return false;
	
	//simulation loop
	for(int n=0;n<nsteps;n++){
		
		//update bias potential if the histogram has converged well enough
		if(counter_nStepsBeforeBiasCheck==nStepsBeforeBiasCheck){
			counter_nStepsBeforeBiasCheck=0;
			if(writeHistogramProgress==true) dumpHistogram("tmp_histogram.dat");
			if(histogramConverged()==true){
				
				double flat=histogramFlattness();
				if(histogramFlattness()<0.3) convergenceThreshold*=0.5;
				
				//write some file output
				dumpConvergenceProgress();
				if(writeHistogramProgress==true){
					std::stringstream filename;
					filename<<"histogram_mcs"
						<<ingredients.getMolecules().getAge()
						<<".dat";
					dumpHistogram(filename.str());
				} 
				
				updateBiasPotential();
				
				//write some more output
				if(writePMFProgress==true) dumpPMF();
				
				//if the simulation has converged, end the simulation
				if(simulationConverged==true) return false;
				
				//set histogram to 0
				resetHistogram();
				
			}
			
		}
		
		
		//do one monte carlo sweep
		for(int m=0;m<ingredients.getMolecules().size();m++)
		{
			move.init(ingredients);
			
			if(move.check(ingredients)==true)
			{
				move.apply(ingredients);
			}
		}
		
		//get update on histogram after every sweep. more often would 
		//probably not make much sense, because the chain diffuses slowly
		try{
			if(counter_nStepsBeforeHistogramUpdate==nStepsBeforeHistogramUpdate){
				counter_nStepsBeforeHistogramUpdate=0;
				histogram.addValue(ingredients.getCurrentReactionCoordinate());
			}
			
		}
		catch(std::runtime_error& e){
			std::stringstream errormessage;
			errormessage<<"UpdaterAdaptiveUmbrellaSampling: error while altering histogram.\n"
			<<"original message was:\n"<<e.what()<<std::endl;
			throw std::runtime_error(errormessage.str());
		}
		
		//increment counter
		counter_nStepsBeforeBiasCheck++;
		counter_nStepsBeforeHistogramUpdate++;
		
	}
	
	//update age of system and give some more output on simulation progress
	ingredients.modifyMolecules().setAge(ingredients.modifyMolecules().getAge()+nsteps);
	std::cout<<"mcs "<<ingredients.getMolecules().getAge() << " with " << (((1.0*nsteps)*ingredients.getMolecules().size())/(std::difftime(std::time(NULL), startTimer)) ) << " [attempted moves/s]" <<std::endl;
	std::cout<<"mcs "<<ingredients.getMolecules().getAge() << " passed time " << ((std::difftime(std::time(NULL), startTimer)) ) << " with " << nsteps << " MCS "<<std::endl;
	
	return true;
}


// initialize only sets the histogram and bias potential to 0
template<class IngredientsType, class MoveType>
void UpdaterAdaptiveUmbrellaSampling<IngredientsType,MoveType>::initialize()
{
	resetHistogram();
	dumpHistogram("initial_histogram.dat");
	dumpPMF();
}


template<class IngredientsType, class MoveType>
void UpdaterAdaptiveUmbrellaSampling<IngredientsType,MoveType>::resetHistogram()
{
	size_t nbins=ingredients.getUmbrellaPotential().getNBins();
	histogram.reset(0.0,ingredients.getBoxZ(),nbins);
}



//checks if mean, variance and deviation from average of the histogram have
//changed more than the percentage threshold. If all has converged, the function
//returns true
template<class IngredientsType, class MoveType>
bool UpdaterAdaptiveUmbrellaSampling<IngredientsType,MoveType>::histogramConverged()
{
	
	bool distributionConverged=distributionCheck();
	bool flatnessConverged=flatnessCheck();
	
	bool converged= distributionConverged && flatnessConverged;
	
	std::cout<<"convergence checks: "<<converged<<std::endl;
		
	return converged;
	
}

//checks if the mean and variance of the reaction coordinate have changed more
//than percentage threshold. also records these two coordinates in the
//histogram convergence vectors for output in cleanup
template<class IngredientsType, class MoveType>
bool UpdaterAdaptiveUmbrellaSampling<IngredientsType,MoveType>::distributionCheck()
{
	//calculate mean and variance
	double mean=0.0;
	double variance=0.0;
	std::vector<double> currentHistogramState=histogram.getVectorValuesNormalized();
	
	for(size_t n=0;n<currentHistogramState.size();n++)
	{
		mean+=currentHistogramState.at(n)*double(n);
		variance+=currentHistogramState.at(n)*double(n)*double(n);
	}
	variance-=mean*mean;
	
	//calculate the relative change with respect to the last check
	double meanValueChange=std::fabs((mean-oldMean)/mean);
	double varianceChange=std::fabs((variance-oldVariance)/variance);
	
	//calculate the relative change with respect to the second to last check
	double meanValueChangeSecond=std::fabs((mean-oldOldMean)/mean);
	double varianceChangeSecond=std::fabs((variance-oldOldVariance)/variance);
	
	if(meanValueChange == std::numeric_limits<double>::infinity()
		||meanValueChange == -std::numeric_limits<double>::infinity()
		||meanValueChange == std::numeric_limits<double>::signaling_NaN()
		||varianceChange == std::numeric_limits<double>::infinity()
		||varianceChange == -std::numeric_limits<double>::infinity()
		||varianceChange == std::numeric_limits<double>::signaling_NaN()
		||meanValueChangeSecond == std::numeric_limits<double>::infinity()
		||meanValueChangeSecond == -std::numeric_limits<double>::infinity()
		||meanValueChangeSecond == std::numeric_limits<double>::signaling_NaN()
		||varianceChangeSecond == std::numeric_limits<double>::infinity()
		||varianceChangeSecond == -std::numeric_limits<double>::infinity()
		||varianceChangeSecond == std::numeric_limits<double>::signaling_NaN())
	{
		std::cerr<<"WARNING: UpdaterAdaptiveUmbrellaSampling finds NaN or inf in convergence check in mcs "
		<<ingredients.getMolecules().getAge()<<"\n";
		return false;
	}
	else if(meanValueChange>convergenceThreshold 
		||varianceChange>convergenceThreshold
		||meanValueChangeSecond>convergenceThreshold
		||varianceChangeSecond>convergenceThreshold){
		std::cout<<"distribution not converged!...\n"
		<<"mean change "<<meanValueChange<<" previous mean change "<<meanValueChangeSecond
		<<"\nvariance change "<<varianceChange<<" previous variance change "<<varianceChangeSecond
		<<"\nthreshold "<<convergenceThreshold<<std::endl;
	        oldOldMean=oldMean;
	        oldOldVariance=oldVariance;
		oldMean=mean;
		oldVariance=variance;
	        varianceSeries.push_back(variance);
	        meanSeries.push_back(mean);
		return false;
	}
	else{
		std::cout<<"distribution converged!...\n"
		<<"mean change "<<meanValueChange<<" previous mean change "<<meanValueChangeSecond
		<<"\nvariance change "<<varianceChange<<" previous variance change "<<varianceChangeSecond
		<<"\nthreshold "<<convergenceThreshold<<std::endl;
		
		oldOldMean=oldMean;
	        oldOldVariance=oldVariance;
		oldMean=mean;
		oldVariance=variance;
	        varianceSeries.push_back(variance);
	        meanSeries.push_back(mean);
		
		return true;
	}
	
}

//checks if the flatness of the histogram, measured by square deviation from 
//mean reached some limiting value (fullConvergenceThreshold), and if it has
//changed more than the percentage threshold (convergenceThreshold) in the
//last two checks. 
template<class IngredientsType, class MoveType>
bool UpdaterAdaptiveUmbrellaSampling<IngredientsType,MoveType>::flatnessCheck()
{
	double currentFlatness=histogramFlattness();
	flatnessSeries.push_back(currentFlatness);
	
	
	double flatnessChange=std::sqrt(std::pow((currentFlatness-oldFlatnesValue)/currentFlatness,2));
	double flatnessChangeSecond=std::sqrt(std::pow((currentFlatness-oldOldFlatnessValue)/currentFlatness,2));
	
	std::cout<<"histogram flatness: "<<currentFlatness<<std::endl;
	std::cout<<"histogram flatness change: "<<flatnessChange<<" old flatness change: "<<flatnessChangeSecond<<std::endl;
	
	oldOldFlatnessValue=oldFlatnesValue;
	oldFlatnesValue=currentFlatness;
	
	if(currentFlatness==0)
	{
		std::cout<<"WARNING...found NaN or Inf in flatness check\n";
	}
		
		if(flatnessChange<=convergenceThreshold && flatnessChangeSecond<=convergenceThreshold && currentFlatness<=fullFlatnessThreshold){
			simulationConverged=true;
		}
// 		else{
// 			std::vector<double> currentHistogram=histogram.getVectorValues();
// 			bool binsHaveReachedMax=true;
// 			for(size_t n=0;n<currentHistogram.size();n++){
// 				binsHaveReachedMax &= (currentHistogram[n]>maxBinsThreshold);
// 			}
// 			
// 			if (binsHaveReachedMax) simulationConverged=true;
// 		}
		
	
	
	
	if(flatnessChange<=convergenceThreshold && flatnessChangeSecond<=convergenceThreshold){
		
		std::cout<<"flatness converged...\n";
		return true;
	}
	else return false;
}

//calculate the measure for the absolute histogram flatness
template<class IngredientsType, class MoveType>
double UpdaterAdaptiveUmbrellaSampling<IngredientsType,MoveType>::histogramFlattness()const 
{
	
	std::vector<double> currentHistogramState=histogram.getVectorValuesNormalized();
	
	//calculate mean
	double mean=1.0/double(currentHistogramState.size());
	double squareDeviation=0.0;
	
	for(size_t n=0;n<currentHistogramState.size();n++)
	{
		squareDeviation+=(currentHistogramState.at(n)-mean)*(currentHistogramState.at(n)-mean)*double(currentHistogramState.size());
	}
	
	squareDeviation=std::sqrt(squareDeviation);
	
	
	return squareDeviation;
	
}


//performs an update on the bias potential based on the current histogram.
//it uses log(histogram/histogramAverage+1) and shifts the bias to zero
//at the lowest point.
template<class IngredientsType, class MoveType>
void UpdaterAdaptiveUmbrellaSampling<IngredientsType,MoveType>::updateBiasPotential()
{
	//perform the update on the pmf
	//first calculate average of the current distribution
	std::cout<<"UpdaterAdaptiveUmbrellaSampling: updating bias potential...";
	
	
	std::vector<double> pmfBins=histogram.getVectorBins();
	std::vector<double> currentDistribution=histogram.getVectorValues();
	std::vector<double> PMFupdate(pmfBins.size());
	
	double average;
	double histgramMinimum=1.0;
	for(size_t n=0;n<currentDistribution.size();n++)
	{
		if(currentDistribution[n]/double(currentDistribution.size()) <histgramMinimum)
			histgramMinimum=currentDistribution[n]/double(currentDistribution.size());
		
		average+=currentDistribution[n]/double(currentDistribution.size());
	}
	
	
	//now update the potential. here I add log(hist/<hist>+epsilon)
	//where epsilon limits the negative values in an update, in case
	//they are not sampled. setting epsilon=0.01 limits it to approx -4kT
	
	double updateLimiter=0.01;
	
	for(size_t n=0;n<currentDistribution.size();n++)
	{
		if(histgramMinimum<updateLimiter)
			PMFupdate[n]=std::log(currentDistribution[n]/average+updateLimiter);
		else
			PMFupdate[n]=std::log(currentDistribution[n]/average);
	}
	//since i know the potential of mean force is symmetric, I symmetrize it 
	//here, to make the convergence faster
	
	for(size_t n=0;n<PMFupdate.size()/2;n++)
	{
		PMFupdate[n]+=PMFupdate[PMFupdate.size()-1-n];
		PMFupdate[n]/=2.0;
		PMFupdate[PMFupdate.size()-1-n]=PMFupdate[n];
	}
	
	//set minimum to 0 and transform the update to ingredients
	for(size_t n=0;n<PMFupdate.size();n++)
	{
		ingredients.modifyUmbrellaPotential().addValue(pmfBins[n],PMFupdate[n]);
	}
	
	//now set the miniumum of the bias potential to zero
	std::vector<double> currentPMF=ingredients.getUmbrellaPotential().getVectorValues();
	double minPMF=currentPMF.at(0);
	for(size_t n=0;n<currentPMF.size();n++)
	{
		if(currentPMF.at(n)<minPMF) minPMF=currentPMF.at(n);
	}
	for(size_t n=0;n<currentPMF.size();n++)
	{
		ingredients.modifyUmbrellaPotential().addValue(pmfBins[n],-minPMF);
	}
	
	std::cout<<"done\n";
	
	nUpdatesPerformed+=1;
}

//write current histogram to file
template<class IngredientsType,class MoveType>
void UpdaterAdaptiveUmbrellaSampling<IngredientsType,MoveType>::dumpHistogram(std::string filename)
{

	std::ofstream file(filename.c_str());	

	std::vector<double> currentHistogram=histogram.getVectorValues();
	for(size_t n=0;n<currentHistogram.size();n++){
		file<<currentHistogram[n]<<"\n";
	}
	file.close();
	
}

//write current bias potential to file
template<class IngredientsType,class MoveType>
void UpdaterAdaptiveUmbrellaSampling<IngredientsType,MoveType>::dumpPMF()
{
	std::stringstream filename;
	filename<<"pmf_mcs"<<ingredients.getMolecules().getAge()<<".dat";
	std::ofstream file(filename.str().c_str());
	
	std::vector<double> currentPMF=ingredients.getUmbrellaPotential().getVectorValues();
	std::vector<double> bins=ingredients.getUmbrellaPotential().getVectorBins();
	for(size_t n=0;n<currentPMF.size();n++){
		file<<bins[n]<<"\t"<<currentPMF[n]<<"\n";
	}
	file.close();
	
}

//write convergence history to file
template<class IngredientsType,class MoveType>
void UpdaterAdaptiveUmbrellaSampling<IngredientsType,MoveType>::dumpConvergenceProgress()
{
	std::stringstream filename;
	filename<<"convergence.dat";
	std::ofstream file;
	file.open(filename.str().c_str(),std::ofstream::out);
       
	
	for(size_t n=0;n<meanSeries.size();n++){
		file<<meanSeries.at(n)<<" "<<varianceSeries.at(n)<<" "<<flatnessSeries.at(n)<<"\n";
	}
	
	file.close();
	
}


#endif

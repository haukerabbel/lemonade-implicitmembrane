#ifndef UPDATER_CREATE_ABCP_H
#define UPDATER_CREATE_ABCP_H

#include<cmath>
#include<vector>

#include <LeMonADE/updater/UpdaterAbstractCreate.h>

template<class IngredientsType>
class UpdaterCreateABCP:public UpdaterAbstractCreate<IngredientsType>
{
public:
	UpdaterCreateABCP(IngredientsType& ing,
				 int32_t typeA,
			  int32_t typeB,
			  uint32_t lengthA,
			  uint32_t lengthB,
			  uint32_t nblocks,
			  uint32_t npolymers=1);
	
	~UpdaterCreateABCP(){}
	
	virtual void initialize();
	virtual bool execute(){return false;}
	virtual void cleanup(){};
	
	
private:
	IngredientsType& ingredients;
	int32_t monoTypeA,monoTypeB;
	uint32_t blockLengthA,blockLengthB,nBlocks,nPolymers;
	
	using UpdaterAbstractCreate<IngredientsType>::addSingleMonomer;
	using UpdaterAbstractCreate<IngredientsType>::addMonomerToParent;
	
};



template<class IngredientsType>
UpdaterCreateABCP<IngredientsType>::UpdaterCreateABCP(IngredientsType& ing,
								    int32_t typeA,
								    int32_t typeB,
								    uint32_t lengthA,
								    uint32_t lengthB,
								    uint32_t nblocks,
								    uint32_t npolymers)
:UpdaterAbstractCreate<IngredientsType>(ing)
,ingredients(ing)
,monoTypeA(typeA)
,monoTypeB(typeB)
,blockLengthA(lengthA)
,blockLengthB(lengthB)
,nBlocks(nblocks)
,nPolymers(npolymers)
{
}

template<class IngredientsType>
void UpdaterCreateABCP<IngredientsType>::initialize()
{
	std::vector<size_t> chainEnds(nPolymers,0);
	std::vector<int32_t> monoTypes;
	std::vector<int32_t> charges;
	
	//generate polymer type sequence
	
	for(size_t n=0;n<nBlocks;n++){
		for(size_t a=0;a<blockLengthA;a++){
			monoTypes.push_back(monoTypeA);
		}
		for(size_t b=0;b<blockLengthB;b++){
			monoTypes.push_back(monoTypeB);
		}
	}
	
	
	//add initial monomers
	for(uint32_t n=0;n<nPolymers;n++){
		//create initial monomer
		if(addSingleMonomer(monoTypes[0])){
			chainEnds[n]=ingredients.getMolecules().size()-1;
		}
		else{
			std::stringstream errormessage;
			errormessage<<"UpdaterCreateABCP::error adding initial monomer for chain no "<<n;
			throw std::runtime_error(errormessage.str());
		}
		
		
		for(uint32_t l=1;l<monoTypes.size();l++){
			
			//create initial monomer
			if(addMonomerToParent(chainEnds[n],monoTypes[l])){
				chainEnds[n]=ingredients.getMolecules().size()-1;
			}
			else{
				std::stringstream errormessage;
				errormessage<<"UpdaterCreateABCP::error adding monomer "<<l<<"to chain no "<<n;
				throw std::runtime_error(errormessage.str());
			}
			
		}
	}
	ingredients.synchronize();
}

#endif /*include guard*/
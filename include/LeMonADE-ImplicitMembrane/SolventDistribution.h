#ifndef LEMONADE_FEATURES_SOLVENT_DISTRIBUTION_H
#define LEMONADE_FEATURES_SOLVENT_DISTRIBUTION_H

class SolventDistribution
{
public:
  virtual uint32_t getTagAt(int32_t x, int32_t y,int32_t z)=0; 
};

class MembraneSolventEnvironment:public SolventDistribution
{
public:
	MembraneSolventEnvironment(int32_t plane,int32_t width):midPlane(plane),halfWidth(width){}
	
	virtual uint32_t getTagAt(int32_t x, int32_t y,int32_t z){
		if(z>=midPlane-halfWidth && z<=midPlane+halfWidth) return 2;
		else return 1;
	}
	
private:
	uint32_t midPlane,halfWidth;
};


class HomogeniousSolventEnvironment:public SolventDistribution
{
public:	
	virtual uint32_t getTagAt(int32_t x, int32_t y,int32_t z){
		return 1;
	}
};

#endif /*LEMONADE_FEATURES_SOLVENT_DISTRIBUTION_H*/

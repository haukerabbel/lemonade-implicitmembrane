#ifndef ANALYZER_AVERAGE_BOND_LENGTH_H
#define ANALYZER_AVERAGE_BOND_LENGTH_H

#include <LeMonADE/analyzer/AbstractAnalyzer.h>
#include <LeMonADE/utility/ResultFormattingTools.h>
#include <LeMonADE/utility/Vector3D.h>

#include <LeMonADE-Extensions/utilities/Histogram1D.h>

/************************************************************************
 * calc average bond length of polymers
 * **********************************************************************/


template < class IngredientsType > class AnalyzerAverageBondLength : public AbstractAnalyzer
{
	typedef typename IngredientsType::molecules_type molecules_type;
	
	const IngredientsType& ingredients;
	
	const std::vector<MonomerGroup<molecules_type> >& groups;
	
	double bSquared, bSquaredSquared,nSamples,numLinks,longestBond;
	
	uint64_t minMcsLimit;
	
	
public:
  
	AnalyzerAverageBondLength(const IngredientsType& ing, const std::vector<MonomerGroup<molecules_type> >& g, uint64_t minMcs);
	
	virtual ~AnalyzerAverageBondLength(){}
	
	virtual void initialize();
	virtual bool execute();
	virtual void cleanup();
	
};

/*************************************************************************
 * implementation of memeber execute()
 * ***********************************************************************/

template<class IngredientsType>
AnalyzerAverageBondLength<IngredientsType>::AnalyzerAverageBondLength(const IngredientsType& ing, 
								      const std::vector<MonomerGroup< molecules_type > >& g,
								      uint64_t minMcs)
:ingredients(ing),groups(g),minMcsLimit(minMcs),bSquared(0.0),bSquaredSquared(0.0),nSamples(0.0),numLinks(0.0),longestBond(0.0)
{
	
}


template< class IngredientsType>
void AnalyzerAverageBondLength<IngredientsType>::initialize()
{	
	for(size_t n=0;n<groups.size();n++)
	{
		for(size_t monoIdx=0;monoIdx<groups[n].size();monoIdx++)
		{
			size_t a=groups[n].trueIndex(monoIdx);
			size_t neighbors=ingredients.getMolecules().getNumLinks(a);
			numLinks+=0.5*double(neighbors);
		}
	}
	
	std::cout<<"AnalyzerAverageBondLength: found "<<numLinks<<" bonds\n";
}

template< class IngredientsType >
bool AnalyzerAverageBondLength<IngredientsType>::execute()
{
	if(ingredients.getMolecules().getAge()>=minMcsLimit)
	{
		nSamples+=1.0;
		for(size_t n=0;n<groups.size();n++)
		{
			for(size_t monoIdx=0;monoIdx<groups[n].size();monoIdx++)
			{
				size_t a=groups[n].trueIndex(monoIdx);
				size_t neighbors=ingredients.getMolecules().getNumLinks(a);
				for(size_t neighbIdx=0;neighbIdx<neighbors;neighbIdx++)
				{
					size_t b=ingredients.getMolecules().getNeighborIdx(a,neighbIdx);
					if(b<a){
						VectorInt3 bond=ingredients.getMolecules()[a]-ingredients.getMolecules()[b];
						double l=bond.getLength();
						if(l>longestBond) longestBond=l;
						bSquared+=l*l;
						bSquaredSquared+=l*l*l*l;
					}
				}
			}
		}
	}
	return true;
}

template<class IngredientsType>
void AnalyzerAverageBondLength<IngredientsType>::cleanup()
{
	
	//now write to file (including a comment)
	std::stringstream comment;
	comment<<"AverageBondLength\n";
	
	comment<<"format: b2 Delta_b2=sqrt(Var(b2)/nSquared)\n";
	
	std::vector<std::vector<double> > result;
	result.resize(3);
	double val=bSquared/nSamples/numLinks;
	double val2=bSquaredSquared/nSamples/numLinks;
	
	
	result[0].push_back(val);
	result[1].push_back(std::sqrt((val2-val*val)/(nSamples*numLinks)));
	result[2].push_back(nSamples*numLinks);
	ResultFormattingTools::writeResultFile("AverageBondLength.dat",ingredients,result,comment.str());
	
	std::cout<<"longest bond "<<longestBond<<std::endl;
	
	
}




#endif


